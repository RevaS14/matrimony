
<!doctype html>
<html lang="en">


<!-- Mirrored from droitthemes.com/html/haqto/home_business_apps.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Apr 2020 10:29:15 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Wedding Bells</title>

    <!-- favicon icon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/slick/slick-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/elegant-icon/elegant-icon-style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/linearicons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/magnify-pop/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/animation/animate.css">
    <link href="http://db.onlinewebfonts.com/c/6b6170fe52fb23f505b4e056fefd2679?family=Pacifico" rel="stylesheet" type="text/css"/>
    <!-- custom -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/wedding-main.css">







<style>
    .haqto_banner_content .banner_title {
      font-size: 48px;
      color: #737373;
  }
  .haqto_banner.haqto_banner_one .overlay {
    background-color: #ffffff;
    background-image: none;
    opacity: 0.8;
    padding-bottom: 20px;
}
.haqto_banner_content .banner_para {
    color: #adadad;
   }
   .haqto_header .header_nav .haqto_header_menu ul li.nav-item a.nav-link {
    color: #040404;
}
.haqto_header .header_nav .haqto_header_menu ul li.nav-item a.nav-link::before {
    height: 3px;
    background: #ff91aa;
}
.haqto_banner_content .email_box .haqto_btn {
    width: 100%;
    border-radius: 0px;
    background: #fff;
    color: black;
    border: 1px solid #ccc;
    /* box-shadow: 0px 0px 35px #ccccccd6; */
    border-left: 4px solid #FF0066;
}
.haqto_banner_content .email_box .haqto_btn:hover {
    box-shadow: 0px 0px 35px #ccccccd6;
}
.haqto_banner_img_content .banner_img {
    position: relative;
    margin-left: -80px;
    display: inline-block;
}
.haqto_banner.haqto_banner_one {
    padding: 170px 0 199px;
    position: relative;
    background: url(assets/images/banner/banner_one_bg_1.png) no-repeat scroll top right;
}
.haqto_footer_social .social_item {
    justify-content: flex-start;
}
.haqto_footer_social .social_item li a {
    color: #989898;
    border: 2px solid #00000057;
}
.haqto_footer_social .social_item li a:hover {
    border-color: #F23264;
    color: #fff;
    background-color: #F23264;
    box-shadow: 0px 10px 10px 3px #ff536c5e;
}


.bg_heart {
    position: relative;
    top: 0;
    left: 0;

    height: 100%;
    overflow: hidden
 }

.heart {
    position: absolute;
    top: -50%;
    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -m-transform: rotate(-45deg);
    transform: rotate(-45deg)
 }

.heart:before {
    position: absolute;
    top: -50%;
    left: 0;
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    background: inherit;
    border-radius: 100%;
}

.heart:after {
    position: absolute;
    top: 0;
    right: -50%;
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    background: inherit;
    border-radius: 100%;
}

@-webkit-keyframes love {
  0%{top:110%}
}
@-moz-keyframes love {
  0%{top:110%}
}
@-ms-keyframes love {
  0%{top:110%}
}
@keyframes love {
  0%{top:110%}
}


.app-store{
    display: inline-block;
    margin-top: 5em;
}
.app-store .play-store {
   margin-right: 20px;
}
.play-store, .apple-store{
    cursor: pointer !important;
}
.app-store .play-store img{
    width: 30%;
}
.app-store .apple-store img{
    width: 26%;
}

.arrow-animate{
    animation: slide1 1s ease-in-out infinite;
    color: #ffffff !important;
    float: right;
    margin-left: 30px;
}

@keyframes slide1 {
    0%,
    100% {
      transform: translate(0, 0);
    }
  
    50% {
      transform: translate(10px, 0);
    }
  }


 

    
</style>

</head>

<body>

    <header class="haqto_header haqto_header_transparent">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <nav class="navbar navbar-expand-lg header_nav">
                        <a class="navbar-brand logo" href="<?php echo base_url(); ?>" style="font-family: Pacifico,cursive;">
                            <!-- <img src="images/logo.png" alt="logo"> -->
                        <label>Wedding Bells</label>
                            <!-- <img src="images/logo-color.png" alt="logo"> -->
                        </a>
                        <button class="navbar-toggler haqto_hamburger" type="button" data-toggle="collapse" data-target="#haqto_header_menu" aria-expanded="false">
                            <span class="bar_icon">
                                <span class="bar bar_1"></span>
                                <span class="bar bar_2"></span>
                                <span class="bar bar_3"></span>
                            </span>
                        </button>

                        <div class="collapse navbar-collapse haqto_header_menu" id="haqto_header_menu">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#"> MY HOME </a>
                                  </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> SEARCH </a>
                                </li>
                              
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> MATCHES <span class="badge badge-success">2</span></a>
                                    <ul class="dropdown-menu matches-dropdown">
                                        <li><a href="#">All Matches <span class="badge badge-success">2</span></a></li>
                                        <li><a href="#">New Matches</a></li>
                                        <li><a href="#">Premium Matches</a></li>
                                        <li><a href="#">Mutual Matches</a></li>
                                        <li><a href="#">Members Looking For You</a></li>
                                        <li><a href="#">Horoscope Matches</a></li>
                                        <li><a href="#">Shortlisted By You</a></li>
                                        <li><a href="#">Ignored Profiles</a></li>
                                        <li style="border: 1px solid #cccccc9c;margin: 10px 0px;"><a href="#"></a></li>
                                        <li><a href="#">Who Viewed You</a></li>
                                        <li><a href="#">Who Shortlisted You</a></li>
                                        <li><a href="#">Who Viewed Your Number</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> MAILBOX </a>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> UPGRADE NOW </a>
                                    <ul class="dropdown-menu matches-dropdown">
                                        <li><a href="#"> Payment Options </a></li>
                                        <li><a href="#"> Assisted Service</a></li>
                                        <li><a href="#">Profile Highlighter</a></li>
                                        <li><a href="#">AstroMatch</a></li>
                                        <li><a href="#">Top Placement</a></li>
                                       
                                        <li style="border: 1px solid #cccccc9c;margin: 10px 0px;"><a href="#"></a></li>
                                        <li><a href="#"> <span class="clr-black1 fw-bold">Call 1800 572 5483</span> to<br> know more about paid services</a></li>
                                      
                                    </ul>
                                </li>
                               
                                <!-- notification -->
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> <img src="<?php echo base_url(); ?>assets/images/envelope.svg" style="width: 25px;"> </a>
                                    <div class="dropdown-menu">
                                       <div class="notification">
                                            <div class="profile-img">
                                                <img src="<?php echo base_url(); ?>assets/images/women-avatar.jpg">
                                            </div>
                                            <div class="notification-content">
                                             <label>   Name and 1 other have viewed your profile.</label>
                                             <div class="view-detail">
                                               <p class="view-all"><a href="">View All</a></p>
                                               <p class="timing">11 Hrs</p>
                                            </div>
                                            </div>
                                       </div>
                                    </div>
                                </li>

                                <!-- profile -->
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> 
                                        <img src="<?php echo base_url(); ?>assets/images/men-avatar.jpg" class="profile-avatar"> 
                                    </a>
                                    <div class="dropdown-menu">
                                       <div class="profile-detail">
                                            <div class="profile-row">
                                                <label class="name-row">Krish S <span>M8298463</span> </label><br>
                                                <label class="account-row">Account type: Free <span><a href=""> Upgrade Account</a></span> </label>
                                            </div>
                                            <div class="profile-row hr-line">
                                                <label class="account-row"> 59% Profile Completeness <span><a href=""> Complete Your Profile</a></span> </label>
                                            </div>
                                            <div class="profile-row hr-line">
                                                <ul class="navbar-nav ml-auto profile-sub-menu">
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#"> Edit Profile </a>
                                                      </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#"> Edit Partner Preferences </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#"> Generate Horoscope </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#"> Get Identity Badge </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#">Settings</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#">Feedback</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="<?php echo base_url(); ?>logout">Logout</a>
                                                    </li>
                                                    </ul>
                                                
                                            </div>
                                       </div>
                                    </div>
                                </li>



                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- haqto_header -->


    <section class="haqto_banner haqto_banner_one bg_heart">


        <div class="overlay"></div>
        <div class="container custom-container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 full_width_1024">
                    <div class="haqto_banner_content">
                       <div class="row">

                           <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                            <div class="profile-section">
                                <div class="main-profile-img">
                                    <img src="<?php echo base_url(); ?>assets/images/men-avatar.jpg">
                                    <div class="add-photo">
                                        <label><a href="">Add Photos</a> </label>
                                    </div>
                                </div>
                                <label class="account-name">Krish S</label>
                                <label class="packs">Flat 19% OFF
                                    on 3 Month Classic Premium pack</label>
                                    <div class="text-center">
                                    <button class="btn upgrade-btn"> Upgrade Membership  </button>
                                </div>



                                <ul class="navbar-nav ml-auto profile-sub-menu content-profile-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> GET TRUST BADGES </a>
                                      </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> EDIT PROFILE </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> EDIT PREFERENCES </a>
                                    </li>
                                </ul>
                                <ul class="navbar-nav ml-auto profile-sub-menu content-profile-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> DAILY RECOMMENDATIONS </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> OUR CHATS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> YOUR CALLS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> YOUR VIEWED MATCHES</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i>  YOUR SHORTLISTED MATCHES</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> YOUR VIEWED NUMBERS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i>    SMS RECEIVED/SENT</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> SETTINGS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> HELP</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> WEDDING SERVICES & PARTNERS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-bullhorn" aria-hidden="true"></i> SUCCESS STORIES</a>
                                    </li>
                                </ul>

                            </div>
                           
                               <div class="profile-content-section">
                                <div class="profile-content-body">

                                    <div class="profile-header">
                                    <div class="card-title">
                                    <label>Complete Your Profile For More Responses</label>
                                </div>

                                <!-- progress bar -->
                                <div class="progress">
                                    <div class="bar" style="width:55%">
                                        <p class="percent">55%</p>
                                    </div>
                                </div>
                            </div>

                                <div class="detail-slider">
                                    <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false">
                                        <!-- The slideshow -->
                                        <div class="carousel-inner" style="height: 140px;">
                                          <div class="carousel-item active">
                                            <div class="slide-body">
                                                <div class="slide-icon">
                                                    <img src="<?php echo base_url(); ?>assets/images/logo.png">
                                                </div>
                                                <div class="slide-content">
                                                       Trust Badges add credibility to your profile.
                                                       <button class="btn slide-btn">Add Identity Badge</button>
                                                </div>
                                            </div>
                                            
                                          </div>
                                          <div class="carousel-item">
                                            <div class="slide-body">
                                                <div class="slide-icon">
                                                    <img src="<?php echo base_url(); ?>assets/images/logo.png">
                                                </div>
                                                <div class="slide-content">
                                                       Trust Badges add credibility to your profile.
                                                       <button class="btn slide-btn">Add Identity Badge</button>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="carousel-item">
                                            <div class="slide-body">
                                                <div class="slide-icon">
                                                    <img src="<?php echo base_url(); ?>assets/images/logo.png">
                                                </div>
                                                <div class="slide-content">
                                                       Trust Badges add credibility to your profile.
                                                       <button class="btn slide-btn">Add Identity Badge</button>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                      
                                        <!-- Left and right controls -->
                                        <div class="left-arrow">
                                            <a class="" href="#demo" data-slide="prev">
                                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                        <div class="right-arrow">
                                            <a class="" href="#demo" data-slide="next">
                                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                      </div>
                                </div>


                                <div class="detail-slider" style="margin: 0; padding: 5px;">
                                    <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false">
                                        <!-- The slideshow -->
                                        <div class="carousel-inner" style="height: 140px;">
                                          <div class="carousel-item active"
                                          style="background: #793d9c url(./assets/images/banner/empty-banner-1.jpg) no-repeat;background-size: cover;padding: 18px 24px;"
                                          >
                                            <div class="slide-body">
                                                <div class="slide-icon">
                                                    <img src="<?php echo base_url(); ?>assets/images/logo.png">
                                                </div>
                                                <div class="slide-content discount-content">
                                                    <label>  Discounts as "Classy" as you are!</label>
                                                    <p>Get a special discount of 19% on 3 Month Classic Premium pack</p>
                                                    <button class="btn upgradenow-btn">UPGRADE NOW</button>
                                                </div>
                                            </div>
                                            
                                          </div>
                                        
                                          
                                        </div>
                                                                             
                                      </div>
                                </div>

                                </div>
                               </div>
                             </div>
                        
                        <div class="col-lg-3 col-md-3 col-sm-3 col-3">
                               <div class="ads-section">

                                   <div class="update-body">
                                    <label>Latest Update</label>
                                 <div class="latest-update">
                                    <div class="partner-img">
                                        <img src="<?php echo base_url(); ?>assets/images/women-avatar.jpg">
                                    </div>
                                    <div class="detail-option">
                                      <label>  Chandni S and 1 other have viewed your profile.</label>
                                      <div class="view-detail update-detail">
                                        <p class="view-all"><a href="">View All</a></p>
                                        <p class="timing">11 Hrs</p>
                                     </div>
                                    </div>
                                 </div>
                                </div>

                                <div class="update-body">
                                    <label>2 Members Viewed Your Profile</label>

                                 <div class="latest-update viewed-profile">
                                    <div class="partner-img">
                                        <img src="<?php echo base_url(); ?>assets/images/women-avatar.jpg">
                                    </div>
                                    <div class="detail-option">
                                      <label>Chandni S<br>
                                        24 yrs, 5'2",<br>
                                        Saliya,<br>
                                        Kozhikode  </label>
                                      <div class="view-detail update-detail">
                                        <p class="view-all"><a href="">View Profile</a></p>
                                     </div>
                                    </div>
                                 </div>

                                 <div class="latest-update viewed-profile">
                                    <div class="partner-img">
                                        <img src="<?php echo base_url(); ?>assets/images/women-avatar.jpg">
                                    </div>
                                    <div class="detail-option">
                                      <label>  Chandni S and 1 other have viewed your profile.</label>
                                      <div class="view-detail update-detail">
                                        <p class="view-all"><a href="">View Profile</a></p>
                                     </div>
                                    </div>
                                 </div>


                                </div>


                               </div>
                        </div>
                       </div>
                    </div>
                </div>
             
            </div>
        </div>
    </section>
   
























    <footer class="haqto_footer_wrap haqto_footer_light haqto_download_footer haqto_health_footer">
        <div class="haqto_footer_bottom">
            <div class="container">
                <div class="haqto_footer_bottom_inner">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                            <div class="haqto_footer_widget">
                                <a href="#" class="haqto_footer_logo">
                                    <!-- <img src="images/logo.png" alt="logo"> -->
                                    <label>Wedding Bells</label>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="haqto_footer_download_menu">
                                <ul class="haqto_footer_menu">
                                    <li><a href="#">Privacy</a></li>
                                    <li><a href="#">Terms</a></li>
                                    <li><a href="#">Reviews</a></li>
                                    <li><a href="#">About </a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                            <div class="haqto_footer_select_language text-right">
                                <select class="select_language">
                                    <option value="0">India - English</option>
                                    <option value="0">United State - English</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- haqto_footer_wrap -->

    <script src="<?php echo base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/slick/slick.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/simple-icon/iconify.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/wow/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/sckroller/jquery.parallax-scroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/magnify-pop/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>


 
<script>
      $(window).scroll(function(){
       
    if ($(window).scrollTop() > 500) {
        // $('.profile-section').addClass('absolute');
        $('.profile-section').removeClass('fixed');
     
    }
    else if ($(window).scrollTop() > 100) {
        $('.profile-section').addClass('fixed');
    }

      else {
        $('.profile-section').removeClass('fixed');
        // $('.profile-section').removeClass('absolute');
    }
});

//document ready
$(document).ready(function(){
    //Event for pushed the video
    $('#demo').carousel({
        pause: true,
        interval: false
    });
});
</script>




<script>
 

var love = setInterval(function() {
    var r_num = Math.floor(Math.random() * 40) + 1;
    var r_size = Math.floor(Math.random() * 65) + 10;
    var r_left = Math.floor(Math.random() * 100) + 1;
    var r_bg = Math.floor(Math.random() * 25) + 100;
    var r_time = Math.floor(Math.random() * 5) + 5;

    $('.bg_heart').append("<div class='heart' style='width:" + r_size + "px;height:" + r_size + "px;left:" + r_left + "%;background:rgba(255," + (r_bg - 25) + "," + r_bg + ",1);-webkit-animation:love " + r_time + "s ease;-moz-animation:love " + r_time + "s ease;-ms-animation:love " + r_time + "s ease;animation:love " + r_time + "s ease'></div>");

    $('.bg_heart').append("<div class='heart' style='width:" + (r_size - 10) + "px;height:" + (r_size - 10) + "px;left:" + (r_left + r_num) + "%;background:rgba(255," + (r_bg - 25) + "," + (r_bg + 25) + ",1);-webkit-animation:love " + (r_time + 5) + "s ease;-moz-animation:love " + (r_time + 5) + "s ease;-ms-animation:love " + (r_time + 5) + "s ease;animation:love " + (r_time + 5) + "s ease'></div>");

    $('.heart').each(function() {
        var top = $(this).css("top").replace(/[^-\d\.]/g, '');
        var width = $(this).css("width").replace(/[^-\d\.]/g, '');
        if (top <= -100 || width >= 150) {
            $(this).detach();
        }
    });
}, 500);
</script>






</body>


</html>