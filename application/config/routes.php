<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'SiteController';
$route['login'] = 'SiteController/login';
$route['register'] = 'SiteController/register';
$route['register-step-first/(:any)'] = 'SiteController/registerStepFirst/$1';
$route['register-step-second/(:any)'] = 'SiteController/registerStepSecond/$1';
$route['register-step-third/(:any)'] = 'SiteController/registerStepThird/$1';
$route['register-step-four/(:any)'] = 'SiteController/registerStepFour/$1';
$route['register-step-five/(:any)'] = 'SiteController/registerStepFive/$1';
$route['get-state'] = 'SiteController/getState';
$route['get-cities'] = 'SiteController/getCities';
$route['myaccount'] = 'SiteController/myaccount';
$route['notify'] = 'SiteController/notify';
$route['logout'] = 'SiteController/logout';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['api'] = 'ApiController/index';
$route['api/register'] = 'ApiController/register';
$route['api/register_step_1'] = 'ApiController/register_step_1';
$route['api/register_step_2'] = 'ApiController/register_step_2';
$route['api/register_step_3'] = 'ApiController/register_step_3';
$route['api/register_step_4'] = 'ApiController/register_step_4';
$route['api/register_step_5'] = 'ApiController/register_step_5';
$route['api/get-user-info'] = 'ApiController/get_user_info';
$route['api/save_basicinfo'] = 'ApiController/saveBasicInfo';

$route['api/recently-joined'] = 'ApiController/recently_joined';
$route['api/generate_order'] = 'ApiController/generateOrder';
$route['api/view_profile'] = 'ApiController/viewProfile';
$route['api/like_profile'] = 'ApiController/likeProfile';
$route['api/unlike_profile'] = 'ApiController/unlikeProfile';
$route['api/like_history'] = 'ApiController/likeHistory';
$route['api/get_my_likes'] = 'ApiController/getMyLikes';

$route['api/shortlist_profile'] = 'ApiController/shortlistProfile';
$route['api/remove_shortlist'] = 'ApiController/removeShortlist';
$route['api/shortlist_history'] = 'ApiController/shortlistHistory';
$route['api/get_my_shortlist'] = 'ApiController/getMyShortlist';

$route['api/location_based_matches'] = 'ApiController/locationBasedMatches';
$route['api/star_based_matches'] = 'ApiController/starBasedMatches';
$route['api/profession_based_matches'] = 'ApiController/professionBasedMatches';
$route['api/education_based_matches'] = 'ApiController/educationBasedMatches';
$route['api/get_premium'] = 'ApiController/getPremium';
$route['api/get_premium_members'] = 'ApiController/getPremiumMembers';
$route['api/update_profile_photo'] = 'ApiController/updateProfile';
$route['api/send_notification'] = 'ApiController/sendFCM';

$route['api/recently_viewed'] = 'ApiController/recentlyViewed';
$route['api/find_matches'] = 'ApiController/findMatch';
$route['api/get_viewers'] = 'ApiController/getViewers';
$route['api/get_preference'] = 'ApiController/getPreference';
$route['api/get_dropdown_values'] = 'ApiController/getDropdownValues';
$route['api/save_preference'] = 'ApiController/save_preference';
$route['api/search_user'] = 'ApiController/search_user';
$route['api/search_history'] = 'ApiController/search_history';
$route['api/users'] = 'ApiController/users';
$route['api/religion_preference'] = 'ApiController/religion_preference';
$route['api/location_preference'] = 'ApiController/location_preference';
$route['api/professional_preference'] = 'ApiController/professional_preference';
$route['api/login'] = 'ApiController/login';
$route['api/phone-number-exists'] = 'ApiController/isPhonenumberExists';
$route['api/delete-user'] = 'ApiController/deleteUser';
