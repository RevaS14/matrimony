
<!doctype html>
<html lang="en">


<!-- Mirrored from droitthemes.com/html/haqto/home_business_apps.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Apr 2020 10:29:15 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Wedding Bells</title>
    <!-- favicon icon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/slick/slick-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/elegant-icon/elegant-icon-style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/linearicons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/magnify-pop/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/animation/animate.css">
    <link href="http://db.onlinewebfonts.com/c/6b6170fe52fb23f505b4e056fefd2679?family=Pacifico" rel="stylesheet" type="text/css"/>
    <!-- custom -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">


    <!-- <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
   
 
 
 
<style>
    .haqto_banner_content .banner_title {
      font-size: 48px;
      color: #737373;
  }
  .haqto_banner.haqto_banner_one .overlay {
    background-color: #ffffff;
    background-image: none;
    opacity: 0.8;
    padding-bottom: 20px;
}
.haqto_banner_content .banner_para {
    color: #adadad;
   }
   .haqto_header .header_nav .haqto_header_menu ul li.nav-item a.nav-link {
    color: #040404;
}
.haqto_header .header_nav .haqto_header_menu ul li.nav-item a.nav-link::before {
    height: 3px;
    background: #ff91aa;
}
.haqto_banner_content .email_box .haqto_btn {
    width: 100%;
    border-radius: 0px;
    background: #fff;
    color: black;
    border: 1px solid #ccc;
    /* box-shadow: 0px 0px 35px #ccccccd6; */
    border-left: 4px solid #FF0066;
}
.haqto_banner_content .email_box .haqto_btn:hover {
    box-shadow: 0px 0px 35px #ccccccd6;
}
.haqto_banner_content{
    background: #ffe1eb;
    height: 100%;
    padding-top: 0px;
}

.haqto_banner_img_content .banner_img {
    position: relative;
    margin-left: -80px;
    display: inline-block;
}
.haqto_banner.haqto_banner_one {
    padding: 170px 0 199px;
    position: relative;
    background: url(images/banner/banner_one_bg_1.png) no-repeat scroll top right;
}
.haqto_footer_social .social_item {
    justify-content: flex-start;
}
.haqto_footer_social .social_item li a {
    color: #989898;
    border: 2px solid #00000057;
}
.haqto_footer_social .social_item li a:hover {
    border-color: #F23264;
    color: #fff;
    background-color: #F23264;
    box-shadow: 0px 10px 10px 3px #ff536c5e;
}


.bg_heart {
    position: relative;
    top: 0;
    left: 0;

    height: 100%;
    overflow: hidden
 }

.heart {
    position: absolute;
    top: -50%;
    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -m-transform: rotate(-45deg);
    transform: rotate(-45deg)
 }

.heart:before {
    position: absolute;
    top: -50%;
    left: 0;
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    background: inherit;
    border-radius: 100%;
}

.heart:after {
    position: absolute;
    top: 0;
    right: -50%;
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    background: inherit;
    border-radius: 100%;
}

@-webkit-keyframes love {
  0%{top:110%}
}
@-moz-keyframes love {
  0%{top:110%}
}
@-ms-keyframes love {
  0%{top:110%}
}
@keyframes love {
  0%{top:110%}
}


.app-store{
    display: inline-block;
    margin-top: 5em;
}
.app-store .play-store {
   margin-right: 20px;
}
.play-store, .apple-store{
    cursor: pointer !important;
}
.app-store .play-store img{
    width: 30%;
}
.app-store .apple-store img{
    width: 26%;
}

.arrow-animate{
    animation: slide1 1s ease-in-out infinite;
    color: #ffffff !important;
    float: right;
    margin-left: 30px;
}

@keyframes slide1 {
    0%,
    100% {
      transform: translate(0, 0);
    }
  
    50% {
      transform: translate(10px, 0);
    }
  }


  .reg-part{
    background: #fff;
    box-shadow: 0px 0px 20px #0000001a;
    padding: 0;
    position: relative;
    z-index: 9;
  }
  .form-child{
    width: 100%;
    margin-bottom: 16px;
  }
  .form-child label{
    float: left;
    width: 150px;
    text-align: right;
    display: block;
    font-size: 20px;
    color: #969696;
    margin-right: 20px;
    margin-top: 5px;
    font-weight: 300;
  }
  .form-content{
    padding-top: 30px;
  }
  .form-content .form-title{
    font-size: 30px;
    font-weight: 300;
    color: #ff5972;
    margin-bottom: 38px;
  }
  .form-child  .form-control, .form-child  .form-control:focus {
      border: none !important;
    border-bottom: 1px solid #ccc !important;
    border-radius: 0 !important;
    box-shadow: none !important;
    width: calc(100% - 50%);
  }

  .foot-btn{
      text-align: center;
  }
  .foot-btn button{
    padding: 10px 70px;
    background: #ffe1eb;
    color: #b3042f;
    font-weight: 500;
    border: none !important;
  }
  .foot-btn button:hover {
    background: #ff667b;
    color: #fff !important;
}
.select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single:focus {
    background-color: #fff;
    border: none !important;
    border-radius: 0px !important;
    border-bottom: 1px solid #ccc !important; 
    outline: none !important;

    display: block;
    width: 100%;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}


.status .selection {
    margin-bottom: 8px;
}

.status .selection label {
    display: inline-block;
    width: auto;
    background-color: #ffffff;
    border-radius: 0px;
    color: #2b2b2b;
    padding: 1px 12px;
    cursor: pointer;
    font-size: 14px;
    margin: 0px 10px;
    border: 1px solid #ff5c8e;
}

.status .selection label:hover {
    background-color: #ffe1eb;
    color: #b30435;
}

.status .selection input[type=radio] {
  display: none;
}

.status .selection input[type=radio]:checked ~ label {
  background-color: #b30435;
  color: #fff;
}



/*Checkboxes styles*/
.boxes input[type="checkbox"] {
  display: none;
}
.boxes {
 
}
.boxes input[type="checkbox"] + label {
  display: inline-block;
  position: relative;
  padding-left: 35px;
  width: 170px;
  margin-bottom: 20px;
  font: 14px/20px "Open Sans", Arial, sans-serif;
  color: #7d7d7d;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}

.boxes input[type="checkbox"] + label:last-child {
  margin-bottom: 0;
}

.boxes input[type="checkbox"] + label:before {
  content: "";
  display: block;
  width: 20px;
  height: 20px;
  border: 1px solid #c50042;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0.6;
  -webkit-transition: all 0.12s, border-color 0.08s;
  transition: all 0.12s, border-color 0.08s;
}

.boxes input[type="checkbox"]:checked + label:before {
  width: 10px;
  top: -5px;
  left: 5px;
  border-radius: 0;
  opacity: 1;
  border-top-color: transparent;
  border-left-color: transparent;
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
}


#NeverMarriedStatus, #OtherStatus{
    display: none;
    margin: 10px 15px;
}
#NoOfChildern{
    display: none;
  }
  #ChildrenLiving{
    display: none;
    margin: 12px 0px;
}
  .float-l{
      float: left;
  }
  .error-alerts {
    margin-left: 180px;
  }
</style>



</head>

<body>

    <header class="haqto_header haqto_header_transparent">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <nav class="navbar navbar-expand-lg header_nav">
                        <a class="navbar-brand logo" href="#" style="font-family: Pacifico,cursive;">
                              <label>Wedding Bells</label>
                       </a>
                        <!-- <button class="navbar-toggler haqto_hamburger" type="button" data-toggle="collapse" data-target="#haqto_header_menu" aria-expanded="false">
                            <span class="bar_icon">
                                <span class="bar bar_1"></span>
                                <span class="bar bar_2"></span>
                                <span class="bar bar_3"></span>
                            </span>
                        </button> -->

                        <!-- <div class="collapse navbar-collapse haqto_header_menu" id="haqto_header_menu">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#"> Home </a>
                                  </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> New Arrival </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> Collections </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> Categories </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="contact.html">Contact Us</a>
                                </li>
                            </ul>
                        </div> -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- haqto_header -->


    <section class="haqto_banner haqto_banner_one bg_heart">


        <div class="overlay"></div>

        
        <div class="container">
            <div class="reg-part">
            <div class="row">
                <div class="col-lg-5 col-md-12 col-sm-12 col-12 full_width_1024">
                    <div class="haqto_banner_content left-content">
                       <img src="<?php echo base_url(); ?>assets/images/banner/step1.png" style="width: 100%;">
                                          
                      
                    </div>
                </div>
               
                <div class="col-lg-7 col-md-12 col-sm-12 col-12 full_width_1024">
                    <div class="haqto_banner_content form-content" style="background: #fff;">
                        <label class="form-title">Tell us about your personal details </label>
                        <form class="form-inline" method="post">
                        
                        <div class="form-child">
                            <label >Marital Status</label>
                            <!-- <input type="text" class="form-control mb-2 mr-sm-6" placeholder="Enter Gothram" id="Gothram"> -->
                            <div class="status">
                                <div class="selection">
                                  <input id="NeverMarried" name="maritalstatus" type="radio" value="Never Married">
                                  <label for="NeverMarried">Never Married</label>
                                </div>
                                <div class="selection">
                                  <input id="Widowed" name="maritalstatus" type="radio" value="Widowed">
                                  <label for="Widowed">Widowed</label>
                                </div>
                                <div class="selection">
                                  <input id="Divorced" name="maritalstatus" type="radio" value="Divorced">
                                  <label for="Divorced">Divorced</label>
                                </div>
                                <div class="selection">
                                    <input id="Awaiting-divorce" name="maritalstatus" type="radio" value="Awaiting divorce">
                                    <label for="Awaiting-divorce">Awaiting divorce</label>
                                  </div>
                              </div>


                              <div class="" id="NeverMarriedStatus">
                                <div class="boxes">
                                   <p>A change in marital status awaits you</p>
                                 </div>
                            </div>
    
                            <div class="" id="OtherStatus">
                                <div class="boxes">
                                   <p>Choose to believe in second chances</p>
                                 </div>
                            </div>
                            <div class="" id="maritalError" style="display:none">
                                <div>
                                   <p class="text-danger">Marital Status Required..</p>
                                 </div>
                            </div>
                        </div>

                        

                        <div class="form-child" id="NoOfChildern">
                            <label >No. of Children</label>
                            <div class="status">
                                <div class="selection">
                                  <input id="None" name="NoOfChildern" type="radio" value="No">
                                  <label for="None">None</label>
                                </div>
                                <div class="selection">
                                  <input id="one" name="NoOfChildern" type="radio" value="1">
                                  <label for="one">1</label>
                                </div>
                                <div class="selection">
                                  <input id="two" name="NoOfChildern" type="radio" value="2">
                                  <label for="two">2</label>
                                </div>
                                <div class="selection">
                                    <input id="three" name="NoOfChildern" type="radio" value="3">
                                    <label for="three">3</label>
                                  </div>
                                  <div class="selection">
                                    <input id="FourAbove" name="NoOfChildern" type="radio" value="4A">
                                    <label for="FourAbove">4 and above</label>
                                  </div>
                              </div>


                              <div class="" id="ChildrenLiving">
                                <div class="status">
                                    <div class="selection float-l">
                                      <input id="WithMe" name="ChildrenLiving" type="radio" value="Y">
                                      <label for="WithMe">Children living with me</label>
                                    </div>
                                    <div class="selection float-l">
                                      <input id="NotWithMe" name="ChildrenLiving" type="radio" value="N">
                                      <label for="NotWithMe">Children not living with me</label>
                                    </div>
                                  </div>
                            
                            </div>
                            
    
                          
                        </div>
                            <div class="error-alerts" id="childError" style="display:none">
                                <div>
                                   <p class="text-danger">No of Children required..</p>
                                 </div>
                            </div>
                            <div class="error-alerts" id="childLivingError" style="display:none">
                                <div>
                                   <p class="text-danger">Children Living required..</p>
                                 </div>
                            </div>



                        <div class="form-child">
                            <label for="Height" >Height</label>
                            <select id="Height" required name="height" class="form-control mb-2 mr-sm-6" > 
                                <option value="">Feet / Inches  </option>
                                <option value="4ft 6in / 137 cms">4ft 6in / 137 cms</option>
                                <option value="4ft 7in / 139 cms">4ft 7in / 139 cms</option>
                                <option value="4ft 8in / 142 cms">4ft 8in / 142 cms</option>
                                <option value="4ft 9in / 144 cms">4ft 9in / 144 cms</option>
                                <option value="4ft 10in / 147 cms">4ft 10in / 147 cms</option>
                                <option value="4ft 11in / 149 cms">4ft 11in / 149 cms</option>
                                <option value="5ft / 152 cms">5ft / 152 cms</option>
                                <option value="5ft 1in / 154 cms">5ft 1in / 154 cms</option>
                                <option value="5ft 2in / 157 cms">5ft 2in / 157 cms</option>
                                <option value="5ft 3in / 160 cms">5ft 3in / 160 cms</option>
                                <option value="5ft 4in / 162 cms">5ft 4in / 162 cms</option>
                                <option value="5ft 5in / 165 cms">5ft 5in / 165 cms</option>
                                <option value="5ft 6in / 167 cms">5ft 6in / 167 cms</option>
                                <option value="5ft 7in / 170 cms">5ft 7in / 170 cms</option>
                                <option value="5ft 8in / 172 cms">5ft 8in / 172 cms</option>
                                <option value="5ft 9in / 175 cms">5ft 9in / 175 cms</option>
                                <option value="5ft 10in / 177 cms">5ft 10in / 177 cms</option>
                                <option value="5ft 11in / 180 cms">5ft 11in / 180 cms</option>
                                <option value="6ft / 182 cms">6ft / 182 cms</option>
                                <option value="6ft 1in / 185 cms">6ft 1in / 185 cms</option>
                                <option value="6ft 2in / 187 cms">6ft 2in / 187 cms</option>
                                <option value="6ft 3in / 190 cms">6ft 3in / 190 cms</option>
                                <option value="6ft 4in / 193 cms">6ft 4in / 193 cms</option>
                                <option value="6ft 5in / 195 cms">6ft 5in / 195 cms</option>
                                <option value="6ft 6in / 198 cms">6ft 6in / 198 cms</option>
                                <option value="6ft 7in / 200 cms">6ft 7in / 200 cms</option>
                                <option value="6ft 8in / 203 cms">6ft 8in / 203 cms</option>
                                <option value="6ft 9in / 205 cms">6ft 9in / 205 cms</option>
                                <option value="6ft 10in / 208 cms">6ft 10in / 208 cms</option>
                                <option value="6ft 11in / 210 cms">6ft 11in / 210 cms</option>
                                <option value="7ft / 213 cms">7ft / 213 cms</option>
                            </select>
                        </div>
                        


                        <div class="form-child">
                            <label >Family Status</label>
                            <div class="status">
                                <div class="selection">
                                  <input id="Middle" name="FamilyStatus" type="radio" value="Middle class">
                                  <label for="Middle">Middle class</label>
                                </div>
                                <div class="selection">
                                  <input id="UpperMiddle" name="FamilyStatus" type="radio" value="Upper middle">
                                  <label for="UpperMiddle">Upper middle class</label>
                                </div>
                                <div class="selection">
                                  <input id="Rich" name="FamilyStatus" type="radio" value="Rich">
                                  <label for="Rich">Rich</label>
                                </div>
                                <div class="selection">
                                    <input id="Affluent" name="FamilyStatus" type="radio" value="Affluent">
                                    <label for="Affluent">Affluent</label>
                                  </div>
                              </div>
                        </div>
                        <div class="error-alerts" id="FamilyStatusError" style="display:none">
                                <div>
                                   <p class="text-danger">Family Status Required..</p>
                                 </div>
                            </div>

                        <div class="form-child">
                            <label >Family Type</label>
                            <div class="status">
                                <div class="selection">
                                  <input id="Joint" name="FamilyType" type="radio" value="Joint">
                                  <label for="Joint">Joint</label>
                                </div>
                                <div class="selection">
                                  <input id="Nuclear" name="FamilyType" type="radio" value="Nuclear">
                                  <label for="Nuclear">Nuclear</label>
                                </div>
                              </div>
                        </div>
                        <div class="error-alerts" id="FamilyTypeError" style="display:none">
                                <div>
                                   <p class="text-danger">Family Type Required..</p>
                                 </div>
                            </div>
                           

                        <div class="form-child">
                            <label >Family Values</label>
                            <div class="status">
                                <div class="selection">
                                  <input id="Orthodox" name="FamilyValues" type="radio" value="Orthodox">
                                  <label for="Orthodox">Orthodox</label>
                                </div>
                                <div class="selection">
                                  <input id="Traditional" name="FamilyValues" type="radio" value="Traditional">
                                  <label for="Traditional">Traditional</label>
                                </div>
                                <div class="selection">
                                  <input id="Moderate" name="FamilyValues" type="radio" value="Moderate">
                                  <label for="Moderate">Moderate</label>
                                </div>
                                <div class="selection">
                                    <input id="Liberal" name="FamilyValues" type="radio" value="Liberal">
                                    <label for="Liberal">Liberal</label>
                                  </div>
                              </div>
                        </div>

                        <div class="error-alerts" id="FamilyValuesError" style="display:none">
                                <div>
                                   <p class="text-danger">Family Values Required..</p>
                                 </div>
                            </div>

                        <div class="form-child">
                            <label >Any Disability</label>
                            <div class="status">
                                <div class="selection">
                                  <input id="Normal" name="Disability" type="radio" value="Normal">
                                  <label for="Normal">Normal</label>
                                </div>
                                <div class="selection">
                                  <input id="Physically" name="Disability" type="radio" value="Physically challenged">
                                  <label for="Physically">Physically challenged</label>
                                </div>
                              </div>
                        </div>

                        <div class="error-alerts" id="DisabilityError" style="display:none">
                                <div>
                                   <p class="text-danger">Disability Required..</p>
                                 </div>
                            </div>


                        <div class="form-child foot-btn">
                            <button type="submit" name="submit" id="btn-3" class="btn btn-primary mb-2 continue-btn"> Continue</button>
                        </div>
                          </form>
                                          
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
   
