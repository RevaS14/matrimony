;
(function ($) {
    "use strict";
    $('.haqto_get_appy_tab_wrap .haqto_get_appy_tab li a').on('click', function (e) {
        var tab_height = $('.haqto_get_appy_tab_slider_item').outerHeight();
        $('.haqto_get_appy_tab_content').css({
            'height': tab_height + 'px',
        });
    });

    /*===========  haqto_feedback_masonry  ===========*/
    function haqto_feedback_masonry() {
        if ($('.haqto_feedback_masonry').length > 0) {
            var $grid = $('.haqto_feedback_masonry').isotope({
                itemSelector: '.haqto_feedback_two_slider_item',
                percentPosition: false,
                layoutMode: 'masonry',
                filter: '.google_play',
                masonry: {
                    // use outer width of grid-sizer for columnWidth
                    columnWidth: 1
                }
            });

            $(".haqto_feedback_two_tab li a").on('click', function () {
                $('.apple_app_store').removeClass('d_none');
                $(".haqto_feedback_two_tab li a").removeClass("active");
                $(this).addClass("active");

                var selector = $(this).attr("data-filter");
                $grid.isotope({
                    filter: selector,
                });
                return false;
            });
        }
    }
    haqto_feedback_masonry();

    function menu_fixed() {
        var h_m_f = $('.haqto_header');

        function menu_scroll_fixed(e) {
            var windowTop = $(window).scrollTop();
            var adDcl = "fixedMenu";
            if (windowTop > 0) {
                e.addClass(adDcl);
            } else {
                e.removeClass(adDcl);
            }
        }
        menu_scroll_fixed(h_m_f);

        $(window).scroll(function () {
            menu_scroll_fixed(h_m_f);
        });
    }
    menu_fixed();


    function haqto_get_appy_tab_slider() {
        if ($('.haqto_get_appy_tab_slider').length > 0) {
            $('.haqto_get_appy_tab_slider').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                speed: 2000,
                arrows: false,
                dots: false,
                autoplay: true,
                pauseOnHover: false,
                responsive: [{
                        breakpoint: 1026,
                        settings: {
                            slidesToShow: 4,
                        }
                    },
                    {
                        breakpoint: 769,
                        settings: {
                            slidesToShow: 3,
                        }
                        },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                        }
                        },
                    {
                        breakpoint: 679,
                        settings: {
                            arrows: false,
                            slidesToShow: 1,
                        }
                        }
                    ]
            });
        }
    }
    haqto_get_appy_tab_slider();

    function haqto_lifestyle_feedback_slider() {
        if ($('.haqto_lifestyle_feedback_slider').length > 0) {
            $('.haqto_lifestyle_feedback_slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 2000,
                speed: 1000,
                arrows: true,
                dots: false,
                pauseOnHover: true,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 640,
                        settings: {
                            arrows: false,
                        }
                        }
                    ]
            });
        }
    }
    haqto_lifestyle_feedback_slider();


    /*--------- WOW js-----------*/
    function wowAnimation() {
        new WOW({
            offset: 100,
            mobile: true
        }).init();
    }
    wowAnimation();

    //js for latest_news_slider
    $('.haqto_review_text_slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        speed: 1000,
        arrows: false,
        centerMode: true,
        centerPadding: '0px',
        dots: false,
        asNavFor: '.haqto_review_slider',
        focusOnSelect: true,
        pauseOnHover: true,
        responsive: [{
                breakpoint: 992,
                settings: {
                    arrows: false,
                    pauseOnHover: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToShow: 3,
                }
                }
            ]
    });

    function haqto_review_slider() {

        //js for latest_news_slider
        $('.haqto_review_slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            speed: 1000,
            arrows: true,
            dots: false,
            asNavFor: '.haqto_review_text_slider',
            pauseOnHover: true,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        arrows: false,
                    }
                    }
                ]
        });
    }
    haqto_review_slider();

    function haqto_video_slider_wrap() {
        //js for haqto_video_slider_wrap
        $('.haqto_video_slider_wrap').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            speed: 2000,
            arrows: false,
            dots: true,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        slidesToShow: 1,
                        dots: false,
                    }
                    }
                ]
        });
    }
    haqto_video_slider_wrap();

    function haqto_travel_video_slider() {
        //js for haqto_travel_video_slider
        $('.haqto_travel_video_slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            speed: 2000,
            arrows: false,
            dots: true,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        dots: false,
                    }
                    }]
        });
    }
    haqto_travel_video_slider();

    function rightClickOFf() {
        /* Right click , ctrl+u and ctrl+shift+i disabled */
        $('body').on('contextmenu', function (e) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        });
    }
    //rightClickOFf();

    function addCladdddssInFAq() {
        $('.haqto_accordion .haqto_faq_title').each(function () {
            var $this = $(this);
            $this.on('click', function (m) {
                var has = $this.parent().hasClass('open');
                $('.haqto_accordion .haqto_faq_title').parent().removeClass('open');
                if (has) {
                    $this.parent().removeClass('open');
                } else {
                    $this.parent().addClass('open');
                }
            });
        });
    }
    addCladdddssInFAq();

    /*----------------------------------------------------
                            Flexslider
    ----------------------------------------------------*/

    function social_slider_screen() {
        if ($(".social_slider_screen").length) {
            $('#sliders').flexslider({
                animation: "slide",
                directionNav: false,
                animationLoop: false,
                manualControls: ".w_items_wrap .w_items",
                slideshow: false,
                touch: true,
            });
        }
    }
    social_slider_screen();

    /*===========Portfolio isotope js===========*/
    function portfolioMasonry() {
        var portfolio = $("#work-portfolio");
        if (portfolio.length) {
            portfolio.imagesLoaded(function () {
                // Activate isotope in container
                portfolio.isotope({
                    itemSelector: ".portfolio_item",
                    layoutMode: 'masonry',
                    filter: "*",
                    animationOptions: {
                        duration: 1000
                    },
                    transitionDuration: '0.5s',
                    masonry: {

                    }
                });

                // Add isotope click function
                $("#portfolio_filter div").on('click', function () {
                    $("#portfolio_filter div").removeClass("active");
                    $(this).addClass("active");

                    var selector = $(this).attr("data-filter");
                    portfolio.isotope({
                        filter: selector,
                        animationOptions: {
                            animationDuration: 750,
                            easing: 'linear',
                            queue: false
                        }
                    });
                    return false;
                });
            });
        }
    }
    portfolioMasonry();

    function popupGallery() {
        if ($(".img_popup,.image-link").length) {
            $(".img_popup,.image-link").each(function () {
                $(".img_popup,.image-link").magnificPopup({
                    type: 'image',
                    tLoading: 'Loading image #%curr%...',
                    removalDelay: 300,
                    mainClass: 'mfp-with-zoom mfp-img-mobile',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image,
                    }
                });
            });
        }
        if ($('.popup-youtube').length) {
            $('.popup-youtube').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
            });
            $('.popup-youtube').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false
            });
        }
    }
    popupGallery();

    function tab_hover() {
        var tab = $(".apz_price_tab");
        if ($(window).width() > 450) {
            if ($(tab).length > 0) {
                tab.append('<li class="hover_bg"></li>');
                if ($('.apz_price_tab li a').hasClass('active_hover')) {
                    var pLeft = $('.apz_price_tab li a.active_hover').position().left,
                        pWidth = $('.apz_price_tab li a.active_hover').css('width');
                    $('.hover_bg').css({
                        left: pLeft,
                        width: pWidth
                    });
                }
                $('.apz_price_tab li a').on('click', function () {
                    $('.apz_price_tab li a').removeClass('active_hover');
                    $(this).addClass('active_hover');
                    var pLeft = $('.apz_price_tab li a.active_hover').position().left,
                        pWidth = $('.apz_price_tab li a.active_hover').css('width');
                    $('.hover_bg').css({
                        left: pLeft,
                        width: pWidth
                    });
                });
            }
        }

    }
    tab_hover();


    function myMap() {
        if ($("#contact_map").length > 0) {
            var mapCanvas = document.getElementById("contact_map");
            var myCenter = new google.maps.LatLng(40.716614, -74.002786);
            var mapOptions = {
                center: myCenter,
                zoom: 13
            };
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({
                position: myCenter,
                icon: 'images/location.png'
            });
            marker.setMap(map);
        }
    }
    myMap();

    /* ===== Parallax Effect===== */
    function parallaxEffect() {
        if ($('.parallax-effect').length) {
            $('.parallax-effect').parallax();
        }
    }
    parallaxEffect();

    function bootstrapTabControl() {
        var i, items = $('.haqto_how_it_tab .nav-link'),
            pane = $('.tab-pane');
        // next
        $('.nexttab').on('click', function () {
            for (i = 0; i < items.length; i++) {
                if ($(items[i]).hasClass('active') === true) {
                    break;
                }
            }
            if (i < items.length - 1) {
                // for tab
                $(items[i]).removeClass('active');
                $(items[i + 1]).addClass('active');
                // for pane
                $(pane[i]).removeClass('active show');
                $(pane[i + 1]).addClass('active show');
            }

        });
        // Prev
        $('.prevtab').on('click', function () {
            for (i = 0; i < items.length; i++) {
                if ($(items[i]).hasClass('active') === true) {
                    break;
                }
            }
            if (i != 0) {
                // for tab
                $(items[i]).removeClass('active');
                $(items[i - 1]).addClass('active');
                // for pane
                $(pane[i]).removeClass('active show');
                $(pane[i - 1]).addClass('active show');
            }
        });
    }
    bootstrapTabControl();


    // Dropdown Menu
    function active_dropdown() {
        if ($(window).width() < 992) {
            $('.haqto_header_menu ul li.nav-item > a.dropdown-toggle').on('click', function (event) {
                event.preventDefault();
                $(this).parent().find('.dropdown-menu').first().toggle(500);
                $(this).parent().siblings().find('.dropdown-menu').hide(500);
            });
        }
    }
    active_dropdown();


    $('.haqto_finance_features_item').on('click', function () {
        var index = $('.haqto_finance_features_item').index(this);
        $('.haqto_finance_features_item').removeClass('active').eq(index).addClass('active');
        $('.screen_changer .screen_bg').removeClass('active').eq(index).addClass('active');
    });

    function feedbackSlider() {
        //js for haqto_travel_video_slider
        $('.feedback_slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            speed: 2000,
            arrows: false,
            dots: true,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                    }
            }]
        });
    }
    feedbackSlider();


    function testimonialSlider() {
        //js for haqto_travel_video_slider
        $('.haqto_testimonial_info').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            speed: 2000,
            arrows: false,
            dots: true,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                    }
            }]
        });
    }
    testimonialSlider();

    function haqto_health_about_slider() {
        $('.haqto_health_about_slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 2000,
            arrows: true,
            dots: false,
            autoplay: true,
            fade: true,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 880,
                    settings: {
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        arrows: false,
                        slidesToShow: 1,
                    }
                }
                ]
        });
    }
    haqto_health_about_slider();

    /*-------------------------------------------------------------------------------
	  MAILCHIMP js
	-------------------------------------------------------------------------------*/
    if ($(".mailchimp").length > 0) {
        $(".mailchimp").ajaxChimp({
            callback: mailchimpCallback,
            url: "http://droitlab.us15.list-manage.com/subscribe/post?u=0fa954b1e090d4269d21abef5&id=a80b5aedb0" //Replace this with your own mailchimp post URL. Don't remove the "". Just paste the url inside "".  
        });
    }
    if ($(".mailchimp_two").length > 0) {
        $(".mailchimp_two").ajaxChimp({
            callback: mailchimpCallback,
            url: "https://droitthemes.us19.list-manage.com/subscribe/post?u=5d334217e146b083fe74171bf&amp;id=0e45662e8c" //Replace this with your own mailchimp post URL. Don't remove the "". Just paste the url inside "".  
        });
    }
    $(".memail").on("focus", function () {
        $(".mchimp-errmessage").fadeOut();
        $(".mchimp-sucmessage").fadeOut();
    });
    $(".memail").on("keydown", function () {
        $(".mchimp-errmessage").fadeOut();
        $(".mchimp-sucmessage").fadeOut();
    });
    $(".memail").on("click", function () {
        $(".memail").val("");
    });

    function mailchimpCallback(resp) {
        if (resp.result === "success") {
            $(".mchimp-errmessage").html(resp.msg).fadeIn(1000);
            $(".mchimp-sucmessage").fadeOut(500);
        } else if (resp.result === "error") {
            $(".mchimp-errmessage").html(resp.msg).fadeIn(1000);
        }
    }

})(jQuery);