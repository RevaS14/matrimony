<?php

class CommonModel extends CI_Model {

    public function __construct() {
      parent::__construct();
      $this->load->database();
    }

    public function _insertData($table,$data){
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }

    public function columnExists($table,$where,$column){
        $this->db->select($column);
        $this->db->from($table);
        $this->db->where($where);
        return $this->db->get()->row();
    }
    

    public function _getData($table,$condition = array(),$deleted='') { 
        $this->db->from($table);
        $this->db->where($condition);
        if($deleted) {
          $where = '(deleted!="1" or deleted Is NULL)';
          $this->db->where($where);
          //$this->db->where('deleted !=',1,False);
        }

        return $this->db->get()->row();
        //return $this->db->get_where($table, $condition)->row();
    }

    public function _getTable($table) {
        return $this->db->get($table)->result();
    }

    public function _getAllData($table,$condition = array(),$deleted='') {
        $this->db->from($table);
        $this->db->where($condition);
        //$this->db->where('deleted !=',1,False);
        if($deleted) {
          $where = '(deleted!="1" or deleted Is NULL)';
          $this->db->where($where);
        }
        return $this->db->get()->result();
      // $this->db->where($condition);
      // return $this->db->get($table)->result();
    }

    public function _getAllDataUser($userId,$where) {  
      $query ="SELECT * , IF((select shortlist_history.id from shortlist_history WHERE shortlist_history.user_id = $userId and shortlist_history.profile_id = users.UserId)>0,'YES','NO') as Shortlisted, 
      IF((select like_history.id from like_history WHERE like_history.user_id = $userId and like_history.profile_id = users.UserId)>0,'YES','NO') as Liked
      FROM users where ".$where."";
      $res = $this->db->query($query);
      return $res->result();
    }


  public function _getHistory($userId) {
         $query ="SELECT vh.*, users.*, IF((select shortlist_history.id from shortlist_history WHERE shortlist_history.user_id = $userId and shortlist_history.profile_id = vh.profile_id)>0,'YES','NO') as Shortlisted, 
         IF((select like_history.id from like_history WHERE like_history.user_id = $userId and like_history.profile_id = vh.profile_id)>0,'YES','NO') as Liked
         FROM view_history as vh
         join users on vh.profile_id = users.UserId
         WHERE vh.id IN (
           SELECT Max(id)
           FROM view_history
           WHERE user_id=".$userId."
           GROUP BY profile_id
         ) ";
        $res = $this->db->query($query);
        return $res->result();
}

public function _getShortlistHistory($userId) {
  $query ="SELECT * , IF((select like_history.id from like_history WHERE like_history.user_id = $userId and profile_id = shortlist_history.profile_id)>0,'YES','NO') as Liked, IF(shortlist_history.id >0,'YES','NO') as Shortlisted
  FROM  shortlist_history
  INNER JOIN users ON shortlist_history.profile_id=users.UserId
  WHERE shortlist_history.user_id = $userId;"; 
  $res = $this->db->query($query);
  return $res->result();
}

public function _getMyShortlist($userId) {
  $query ="SELECT * , IF((select like_history.id from like_history WHERE like_history.user_id = $userId and like_history.profile_id = shortlist_history.user_id)>0,'YES','NO') as Liked, IF(shortlist_history.id >0,'YES','NO') as Shortlisted
  FROM  shortlist_history
  INNER JOIN users ON shortlist_history.user_id=users.UserId
  WHERE shortlist_history.profile_id = $userId;"; 
  $res = $this->db->query($query);
  return $res->result();
}

public function _getLikeHistory($userId) {
  $query ="SELECT * , IF((select shortlist_history.id from shortlist_history WHERE shortlist_history.user_id = $userId and profile_id = like_history.profile_id)>0,'YES','NO') as Shortlisted, IF(like_history.id >0,'YES','NO') as Liked
  FROM  like_history
  INNER JOIN users ON like_history.profile_id=users.UserId
  WHERE like_history.user_id = $userId;"; 
  $res = $this->db->query($query);
  return $res->result();
}

public function _getMyLikes($userId) {
  $query ="SELECT * , IF((select shortlist_history.id from shortlist_history WHERE shortlist_history.user_id = $userId and shortlist_history.profile_id = like_history.user_id)>0,'YES','NO') as Shortlisted, IF(like_history.id >0,'YES','NO') as Liked
  FROM  like_history
  INNER JOIN users ON like_history.user_id=users.UserId
  WHERE like_history.profile_id = $userId;"; 
  $res = $this->db->query($query);
  return $res->result();
}



public function _getViewers($userId) {
        $query ="SELECT * , IF((select shortlist_history.id from shortlist_history WHERE shortlist_history.user_id = $userId and shortlist_history.profile_id = vh.user_id)>0,'YES','NO') as Shortlisted, 
        IF((select like_history.id from like_history WHERE like_history.user_id = $userId and like_history.profile_id = vh.user_id)>0,'YES','NO') as Liked
        FROM view_history as vh
        join users on vh.user_id = users.UserId
        WHERE vh.id IN (
          SELECT Max(id)
          FROM view_history
          WHERE profile_id=".$userId."
          GROUP BY user_id
        ) ";
      $res = $this->db->query($query);
      return $res->result();
}

    public function _getRecentUsers() {
      $date_start = strtotime('last Sunday');
      $week_start = date('Y-m-d', $date_start);
      $date_end = strtotime('next Sunday');
      $week_end = date('Y-m-d', $date_end);
      $this->db->from('users');
      $this->db->where('CreatedOn >=', $week_start);
      $this->db->where('CreatedOn <=', $week_end);
      return $this->db->get()->result();
    // $this->db->where($condition);
    // return $this->db->get($table)->result();
  }

    public function _updateData($table,$data,$condition) {
        $this->db->update($table, $data, $condition);
        return true;
    }

    public function _getDetail($table,$where) {
		return $this->db->get_where($table, $where)->row();
    }

    public function _deleteData($table,$where) {
      $this->db->where($where);
      $this->db->delete($table);
    }
}