<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SiteController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('CommonModel');
    }

    public function index() {
		$this->render_page('site/index');
    }

    public function registerStepFirst($id) {
        $this->securePage();
        $login = $this->input->post('submit');
        if(isset($login)) {
            $dob = $this->input->post('dob');
            $religion = $this->input->post('religion');
            $motherTongue = $this->input->post('motherTongue');
            $curlData = array(
                "DOB" => $dob,
                "Religion" => $religion,
                "MotherTongue" => $motherTongue,
            );
            $url = 'https://emchat.live/index.php/api/register_step_1';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curlData));
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'aftership-api-key: 1a3c6af6-fa6f-4b41-bcd4-94fb63ac6bfa',
                'Content-Type: application/json',
                'X-Authorization: '.$this->session->userdata('token').''
                ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            
            $result = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($result);
            if($result->status===true) {
                    redirect('register-step-second/'.$id);
            } else {
                $this->session->set_flashdata('errorMessage',$result->msg);
            }  
        }
		$this->load->view('site/stepFirst');
		$this->load->view('site/footer');
    }

    public function registerStepSecond($id) {
        $this->securePage();
        //echo $this->session->userdata('token');
        $data['dosham'] = $this->CommonModel->_getTable('dosham');
        $data['caste'] = $this->CommonModel->_getTable('caste');
        $login = $this->input->post('submit');
        if(isset($login)) {
            $caste = $this->input->post('caste');
            $subcaste = $this->input->post('subcaste');
            $gothram = $this->input->post('gothram');
            $doshamType = $this->input->post('dosham');

            $curlData = array();
            $curlData['Caste'] = $caste;
            $curlData['SubCaste'] = $subcaste;
            $curlData['Gothram'] = $gothram;
            $curlData['Dosham'] = $doshamType;
            $doshams = $this->input->post('doshamName');
            $curlData['Doshams'] = $doshams;
  
            $url = base_url().'index.php/api/register_step_2';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curlData));
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'aftership-api-key: 1a3c6af6-fa6f-4b41-bcd4-94fb63ac6bfa',
                'Content-Type: application/json',
                'X-Authorization: '.$this->session->userdata('token').''
                ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            
            $result = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($result);
            if($result->status===true) {
                    redirect('register-step-third/'.$id);
            } else {
                $this->session->set_flashdata('errorMessage',$result->msg);
            }  
        }
		$this->load->view('site/stepSecond',$data);
		$this->load->view('site/footer');
    }

    public function registerStepThird($id) {
        $this->securePage();
        //echo $this->session->userdata('token');
        $login = $this->input->post('submit');
        if(isset($login)) {
            $maritalstatus = $this->input->post('maritalstatus');
            $NoOfChildern = $this->input->post('NoOfChildern');
            $ChildrenLiving = $this->input->post('ChildrenLiving');
            $height = $this->input->post('height');
            $FamilyStatus = $this->input->post('FamilyStatus');
            $FamilyType = $this->input->post('FamilyType');
            $FamilyValues = $this->input->post('FamilyValues');
            $Disability = $this->input->post('Disability');

            $curlData = array();
            $curlData['MaritalStatus'] = $maritalstatus;
            $curlData['NoOfChildrens'] = $NoOfChildern;
            $curlData['ChildrensLiveWith'] = $ChildrenLiving;
            $curlData['Height'] = $height;
            $curlData['FamilyStatus'] = $FamilyStatus;
            $curlData['FamilyType'] = $FamilyType;
            $curlData['FamilyValues'] = $FamilyValues;
            $curlData['AnyDisability'] = $Disability;
  
            $url = base_url().'index.php/api/register_step_3';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curlData));
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'aftership-api-key: 1a3c6af6-fa6f-4b41-bcd4-94fb63ac6bfa',
                'Content-Type: application/json',
                'X-Authorization: '.$this->session->userdata('token').''
                ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            
            $result = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($result);
            if($result->status===true) {
                    redirect('register-step-four/'.$id);
            } else {
                $this->session->set_flashdata('errorMessage',$result->msg);
            }  
        }
		$this->load->view('site/stepThird');
		$this->load->view('site/footer');
    }

    public function getEducations($id) {
        return $this->CommonModel->_getAllData('educations',array('status'=>'A','Category'=>$id));
    }

    public function getOccupations($id) {
        return $this->CommonModel->_getAllData('occupations',array('status'=>'A','Category'=>$id));
    }

    public function getState() {
        $data = $this->CommonModel->_getAllData('states',array('country_id'=>$this->input->post('id')));
        echo json_encode($data);
    }

    public function getCities() {
        $data = $this->CommonModel->_getAllData('cities',array('state_id'=>$this->input->post('id')));
        echo json_encode($data);
    }

    
    public function registerStepFour($id) {
        $this->securePage();
        //echo $this->session->userdata('token');
        $data['controller'] = $this;
        $data['countries'] = $this->CommonModel->_getAllData('countries');
        $data['currency'] = $this->CommonModel->_getAllData('currency',array('status'=>'A'));
        $data['income'] = $this->CommonModel->_getAllData('income',array('status'=>'A'));
        $data['category'] = $this->CommonModel->_getAllData('education_category',array('status'=>'A'));
        $data['occupationCategory'] = $this->CommonModel->_getAllData('occupation_category',array('status'=>'A'));
        $login = $this->input->post('submit');
        if(isset($login)) {
            $education = $this->input->post('education');
            $employeed = $this->input->post('Employed');
            $occupation = $this->input->post('occupation');
            $incomeCurrency = $this->input->post('incomeCurrency');
            $income = $this->input->post('income');
            $country = $this->input->post('country');
            $state = $this->input->post('state');
            $city = $this->input->post('city');

            $curlData = array();
            $curlData['Education'] = $education;
            $curlData['EmployedIn'] = $employeed;
            $curlData['Occupation'] = $occupation;
            $curlData['AnnualIncomeCurrency'] = $incomeCurrency;
            $curlData['AnnualIncome'] = $income;
            $curlData['WorkLocation'] = $country;
            $curlData['State'] = $state;
            $curlData['City'] = $city;
  
            $url = base_url().'index.php/api/register_step_4';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curlData));
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'aftership-api-key: 1a3c6af6-fa6f-4b41-bcd4-94fb63ac6bfa',
                'Content-Type: application/json',
                'X-Authorization: '.$this->session->userdata('token').''
                ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            
            $result = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($result);
            if($result->status===true) {
                    redirect('register-step-five/'.$id);
            } else {
                $this->session->set_flashdata('errorMessage',$result->msg);
            }  
        }
		$this->load->view('site/stepFour',$data);
		$this->load->view('site/footer');
    }

    public function registerStepFive($id) {
        $this->securePage();
        //echo $this->session->userdata('token');
        $login = $this->input->post('submit');
        if(isset($login)) {
            $description = $this->input->post('description');

            $curlData = array();
            $curlData['EducationDescription'] = $description;
          
            $url = base_url().'index.php/api/register_step_5';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curlData));
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'aftership-api-key: 1a3c6af6-fa6f-4b41-bcd4-94fb63ac6bfa',
                'Content-Type: application/json',
                'X-Authorization: '.$this->session->userdata('token').''
                ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            
            $result = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($result);
            if($result->status===true) {
                    redirect('complete-profile/'.$id);
            } else {
                $this->session->set_flashdata('errorMessage',$result->msg);
            }  
        }
		$this->load->view('site/stepFive');
		$this->load->view('site/footer');
    }

    public function login() {
        $login = $this->input->post('login');
        if(isset($login)) {
            $name = $this->input->post('mobile');
            $password = $this->input->post('password');
            $curlData = array(
                "MobileNumber" => $name,
                "Password" => $password,
                "UID" =>''
            );
            $url = 'https://emchat.live/index.php/api/login';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curlData));
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'aftership-api-key: 1a3c6af6-fa6f-4b41-bcd4-94fb63ac6bfa',
                'Content-Type: application/json'
                ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            
            $result = curl_exec($curl);

            curl_close($curl);
            $result = json_decode($result);
            if($result->status) {
                $this->session->set_userdata(array(
                    'user_id'  => $result->userId,
                    'username' => $result->userName,
                    'token' => $result->token,
                    'user_logged_in' => TRUE
                ));
                redirect('myaccount');
            } else {
                $this->session->set_flashdata('errorMessage','Invalid Credentials..');
            }           
        }
        $this->load->view('site/login');
    }

    public function register() {
            $name = $this->input->post('name');
            $mobile = $this->input->post('mobile');
            $password = $this->input->post('password');
            $gender = $this->input->post('gender_for');
            $profileFor = $this->input->post('profileFor');
            $curlData = array(
                "UserName" => $name,
                "MobileNumber" => $mobile,
                "ProfileFor" => $profileFor,
                "Gender" => $gender,
                "Password" => $password,
            );
            $url = 'https://emchat.live/index.php/api/register';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curlData));
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'aftership-api-key: 1a3c6af6-fa6f-4b41-bcd4-94fb63ac6bfa',
                'Content-Type: application/json'
                ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            
            $result = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($result);
            if($result->status) {
                $this->session->set_userdata(array(
                    'user_id'  => $result->userId,
                    'user_logged_in' => TRUE,
                    'token' => $result->token
                ));
                redirect('register-step-first/'.$result->userId);
            } else {
                $this->session->set_flashdata('errorMessage',$result->message);
                redirect('/');
            }           
        //$this->load->view('site/login');
    }

    public function notify() {
        if(isset($_POST['razorpay_order_id'])){
            $where = array(
                "txn_id" => $_POST['razorpay_order_id']
            );
            $data = $this->CommonModel->_getData('transactions',$where);
            if($data) {
                $this->CommonModel->_updateData('transactions',array('status'=>'S'),$where);
                $this->CommonModel->_updateData('users',array('Premium'=>'Y'),array('UserId'=>$data->user_id));
            }
        }
    }

    public function myaccount() {
        $this->securePage();
        $this->load->view('site/myaccount');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }

    protected function render_page($view,$data=array()) {
        $data['controller'] = $this;
		$this->load->view('site/header', $data);
		$this->load->view($view, $data);
		$this->load->view('site/footer', $data);
    }
    
    public function securePage() {
        if(!$this->session->userdata('user_id')) {
            redirect('login');
        }
    }
}

?>