
<!doctype html>
<html lang="en">


<!-- Mirrored from droitthemes.com/html/haqto/home_business_apps.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Apr 2020 10:29:15 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Wedding Bells</title>

    <!-- favicon icon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/slick/slick-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/elegant-icon/elegant-icon-style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/linearicons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/magnify-pop/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/animation/animate.css">
    <link href="http://db.onlinewebfonts.com/c/6b6170fe52fb23f505b4e056fefd2679?family=Pacifico" rel="stylesheet" type="text/css"/>
    <!-- custom -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">


    <!-- <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
   
 

 
<style>
    .haqto_banner_content .banner_title {
      font-size: 48px;
      color: #737373;
  }
  .haqto_banner.haqto_banner_one .overlay {
    background-color: #ffffff;
    background-image: none;
    opacity: 0.8;
    padding-bottom: 20px;
}
.haqto_banner_content .banner_para {
    color: #adadad;
   }
   .haqto_header .header_nav .haqto_header_menu ul li.nav-item a.nav-link {
    color: #040404;
}
.haqto_header .header_nav .haqto_header_menu ul li.nav-item a.nav-link::before {
    height: 3px;
    background: #ff91aa;
}
.haqto_banner_content .email_box .haqto_btn {
    width: 100%;
    border-radius: 0px;
    background: #fff;
    color: black;
    border: 1px solid #ccc;
    /* box-shadow: 0px 0px 35px #ccccccd6; */
    border-left: 4px solid #FF0066;
}
.haqto_banner_content .email_box .haqto_btn:hover {
    box-shadow: 0px 0px 35px #ccccccd6;
}
.haqto_banner_content{
    background: #ffe1eb;
    height: 100%;
    padding-top: 0px;
}

.haqto_banner_img_content .banner_img {
    position: relative;
    margin-left: -80px;
    display: inline-block;
}
.haqto_banner.haqto_banner_one {
    padding: 170px 0 199px;
    position: relative;
    background: url(images/banner/banner_one_bg_1.png) no-repeat scroll top right;
}
.haqto_footer_social .social_item {
    justify-content: flex-start;
}
.haqto_footer_social .social_item li a {
    color: #989898;
    border: 2px solid #00000057;
}
.haqto_footer_social .social_item li a:hover {
    border-color: #F23264;
    color: #fff;
    background-color: #F23264;
    box-shadow: 0px 10px 10px 3px #ff536c5e;
}
.textarea{
    height: 150px !important;
    border: 1px solid #ccc !important;
}

.bg_heart {
    position: relative;
    top: 0;
    left: 0;

    height: 100%;
    overflow: hidden
 }

.heart {
    position: absolute;
    top: -50%;
    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -m-transform: rotate(-45deg);
    transform: rotate(-45deg)
 }

.heart:before {
    position: absolute;
    top: -50%;
    left: 0;
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    background: inherit;
    border-radius: 100%;
}

.heart:after {
    position: absolute;
    top: 0;
    right: -50%;
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    background: inherit;
    border-radius: 100%;
}

@-webkit-keyframes love {
  0%{top:110%}
}
@-moz-keyframes love {
  0%{top:110%}
}
@-ms-keyframes love {
  0%{top:110%}
}
@keyframes love {
  0%{top:110%}
}


.app-store{
    display: inline-block;
    margin-top: 5em;
}
.app-store .play-store {
   margin-right: 20px;
}
.play-store, .apple-store{
    cursor: pointer !important;
}
.app-store .play-store img{
    width: 30%;
}
.app-store .apple-store img{
    width: 26%;
}

.arrow-animate{
    animation: slide1 1s ease-in-out infinite;
    color: #ffffff !important;
    float: right;
    margin-left: 30px;
}

@keyframes slide1 {
    0%,
    100% {
      transform: translate(0, 0);
    }
  
    50% {
      transform: translate(10px, 0);
    }
  }


  .reg-part{
    background: #fff;
    box-shadow: 0px 0px 20px #0000001a;
    padding: 0;
    position: relative;
    z-index: 9;
  }
  .form-child{
    width: 100%;
    margin-bottom: 16px;
  }
  .form-child label{
    float: left;
    width: 200px;
    text-align: right;
    display: block;
    font-size: 20px;
    color: #969696;
    margin-right: 20px;
    margin-top: 5px;
    font-weight: 300;
  }
  .form-content{
    padding-top: 30px;
  }
  .form-content .form-title{
    font-size: 30px;
    font-weight: 300;
    color: #ff5972;
    margin-bottom: 38px;
  }
  .form-child  .form-control, .form-child  .form-control:focus {
      /* border: none !important; */
    border-bottom: 1px solid #ccc !important;
    border-radius: 0 !important;
    box-shadow: none !important;
    width: calc(100% - 36%);
  }

  .foot-btn{
      text-align: center;
  }
  .foot-btn button{
    padding: 10px 70px;
    background: #ffe1eb;
    color: #b3042f;
    font-weight: 500;
    border: none !important;
  }
  .foot-btn button:hover {
    background: #ff667b;
    color: #fff !important;
}
.select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single:focus {
    background-color: #fff;
    border: none !important;
    border-radius: 0px !important;
    border-bottom: 1px solid #ccc !important; 
    outline: none !important;

    display: block;
    width: 100%;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}


.dosham .selection {
    margin-bottom: 8px;
}

.dosham .selection label {
    display: inline-block;
    width: auto;
    background-color: #ffffff;
    border-radius: 0px;
    color: #2b2b2b;
    padding: 1px 12px;
    cursor: pointer;
    font-size: 14px;
    margin: 0px 10px;
    border: 1px solid #ff5c8e;
}

.dosham .selection label:hover {
    background-color: #ffe1eb;
    color: #b30435;
}

.dosham .selection input[type=radio] {
  display: none;
}

.dosham .selection input[type=radio]:checked ~ label {
  background-color: #b30435;
  color: #fff;
}



/*Checkboxes styles*/
.boxes input[type="checkbox"] {
  display: none;
}
.boxes {
 
}
.boxes input[type="checkbox"] + label {
  display: inline-block;
  position: relative;
  padding-left: 35px;
  width: 170px;
  margin-bottom: 20px;
  font: 14px/20px "Open Sans", Arial, sans-serif;
  color: #7d7d7d;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}

.boxes input[type="checkbox"] + label:last-child {
  margin-bottom: 0;
}

.boxes input[type="checkbox"] + label:before {
  content: "";
  display: block;
  width: 20px;
  height: 20px;
  border: 1px solid #c50042;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0.6;
  -webkit-transition: all 0.12s, border-color 0.08s;
  transition: all 0.12s, border-color 0.08s;
}

.boxes input[type="checkbox"]:checked + label:before {
  width: 10px;
  top: -5px;
  left: 5px;
  border-radius: 0;
  opacity: 1;
  border-top-color: transparent;
  border-left-color: transparent;
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
}


#yes-dosham{
    display: none;
    width: 100%;
    background: #ffe1eb;
    margin: 10px 30px;
    padding: 8px 14px;
}


.select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single:focus {
    background-color: #fff;
    border: none !important;
    border-radius: 0px !important;
    border-bottom: 1px solid #ccc !important; 
    outline: none !important;

    display: block;
    width: 100%;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}


.status .selection {
    margin-bottom: 8px;
}

.status .selection label {
    display: inline-block;
    width: auto;
    background-color: #ffffff;
    border-radius: 0px;
    color: #2b2b2b;
    padding: 1px 12px;
    cursor: pointer;
    font-size: 14px;
    margin: 0px 10px;
    border: 1px solid #ff5c8e;
}

.status .selection label:hover {
    background-color: #ffe1eb;
    color: #b30435;
}

.status .selection input[type=radio] {
  display: none;
}

.status .selection input[type=radio]:checked ~ label {
  background-color: #b30435;
  color: #fff;
}
#EmployedSection .status .selection{
    margin-bottom: 0px;
}
#EmployedSection .status .selection label{
    margin: 4px 4px;
}
.education-select{
    display: none;
}

.character{
    float: right !important;
    font-size: 14px !important;
    margin: 0px 20px !important;
}

</style>



</head>

<body>

    <header class="haqto_header haqto_header_transparent">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <nav class="navbar navbar-expand-lg header_nav">
                        <a class="navbar-brand logo" href="#" style="font-family: Pacifico,cursive;">
                              <label>Wedding Bells</label>
                       </a>
                        <!-- <button class="navbar-toggler haqto_hamburger" type="button" data-toggle="collapse" data-target="#haqto_header_menu" aria-expanded="false">
                            <span class="bar_icon">
                                <span class="bar bar_1"></span>
                                <span class="bar bar_2"></span>
                                <span class="bar bar_3"></span>
                            </span>
                        </button> -->

                        <!-- <div class="collapse navbar-collapse haqto_header_menu" id="haqto_header_menu">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#"> Home </a>
                                  </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> New Arrival </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> Collections </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> Categories </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="contact.html">Contact Us</a>
                                </li>
                            </ul>
                        </div> -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- haqto_header -->


    <section class="haqto_banner haqto_banner_one bg_heart">


        <div class="overlay"></div>

        
        <div class="container">
            <div class="reg-part">
            <div class="row">
                <div class="col-lg-5 col-md-12 col-sm-12 col-12 full_width_1024">
                    <div class="haqto_banner_content left-content">
                       <img src="<?php echo base_url(); ?>assets/images/banner/step1.png" style="width: 100%;">
                                          
                      
                    </div>
                </div>
               
                <div class="col-lg-7 col-md-12 col-sm-12 col-12 full_width_1024">
                    <div class="haqto_banner_content form-content" style="background: #fff;">
                        <label class="form-title">Let the world know about your awesomeness </label>
                        <form class="form-inline" method="post">
                          



                            <div class="form-child">
                                <label for="education" >Highest Education</label>
                                <textarea name="description" required class="form-control mb-2 mr-sm-6 textarea" placeholder="Write a few words"></textarea>
                                <label class="character">253 Characters Typed.</label>
                            </div>

                            <div class="form-child foot-btn">
                            <button class="btn btn-primary mb-2 continue-btn" type="submit" name="submit">Complete</button>
                        </div>
                          </form>
                                          
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
   
