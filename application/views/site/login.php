
<!doctype html>
<html lang="en">


<!-- Mirrored from droitthemes.com/html/haqto/home_business_apps.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Apr 2020 10:29:15 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Wedding Bells</title>

    <!-- favicon icon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/slick/slick-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/elegant-icon/elegant-icon-style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/linearicons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/magnify-pop/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/animation/animate.css">
    <link href="http://db.onlinewebfonts.com/c/6b6170fe52fb23f505b4e056fefd2679?family=Pacifico" rel="stylesheet" type="text/css"/>
    <!-- custom -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">


    <!-- <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
   
 
 
 

 
<style>
    .haqto_banner_content .banner_title {
      font-size: 48px;
      color: #737373;
  }
  .haqto_banner.haqto_banner_one .overlay {
    background-color: #ffffff;
    background-image: none;
    opacity: 0;
    padding-bottom: 20px;
}
.haqto_banner_content .banner_para {
    color: #adadad;
   }
   .haqto_header .header_nav .haqto_header_menu ul li.nav-item a.nav-link {
    color: #040404;
}
.haqto_header .header_nav .haqto_header_menu ul li.nav-item a.nav-link::before {
    height: 3px;
    background: #ff91aa;
}
.haqto_banner_content .email_box .haqto_btn {
    width: 100%;
    border-radius: 0px;
    background: #fff;
    color: black;
    border: 1px solid #ccc;
    /* box-shadow: 0px 0px 35px #ccccccd6; */
    border-left: 4px solid #FF0066;
}
.haqto_banner_content .email_box .haqto_btn:hover {
    box-shadow: 0px 0px 35px #ccccccd6;
}
.haqto_banner_content{
    background: #ffe1eb;
    height: 100%;
    padding-top: 0px;
}

.haqto_banner_img_content .banner_img {
    position: relative;
    margin-left: -80px;
    display: inline-block;
}
.haqto_banner.haqto_banner_one {
    padding: 0;
    position: relative;
    background: url(assets/images/banner/login-bg1.png) no-repeat scroll top center;
    background-size: cover;
    height: 100vh;
}
.haqto_footer_social .social_item {
    justify-content: flex-start;
}
.haqto_footer_social .social_item li a {
    color: #989898;
    border: 2px solid #00000057;
}
.haqto_footer_social .social_item li a:hover {
    border-color: #F23264;
    color: #fff;
    background-color: #F23264;
    box-shadow: 0px 10px 10px 3px #ff536c5e;
}
.textarea{
    height: 150px !important;
    border: 1px solid #ccc !important;
}

.bg_heart {
    position: relative;
    top: 0;
    left: 0;

    height: 100%;
    overflow: hidden
 }

.heart {
    position: absolute;
    top: -50%;
    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -m-transform: rotate(-45deg);
    transform: rotate(-45deg)
 }

.heart:before {
    position: absolute;
    top: -50%;
    left: 0;
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    background: inherit;
    border-radius: 100%;
}

.heart:after {
    position: absolute;
    top: 0;
    right: -50%;
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    background: inherit;
    border-radius: 100%;
}

@-webkit-keyframes love {
  0%{top:110%}
}
@-moz-keyframes love {
  0%{top:110%}
}
@-ms-keyframes love {
  0%{top:110%}
}
@keyframes love {
  0%{top:110%}
}


.app-store{
    display: inline-block;
    margin-top: 5em;
}
.app-store .play-store {
   margin-right: 20px;
}
.play-store, .apple-store{
    cursor: pointer !important;
}
.app-store .play-store img{
    width: 30%;
}
.app-store .apple-store img{
    width: 26%;
}

.arrow-animate{
    animation: slide1 1s ease-in-out infinite;
    color: #ffffff !important;
    float: right;
    margin-left: 30px;
}

@keyframes slide1 {
    0%,
    100% {
      transform: translate(0, 0);
    }
  
    50% {
      transform: translate(10px, 0);
    }
  }






  .reg-part{
    /* background: #fff;
    box-shadow: 0px 0px 20px #0000001a; */
    padding: 0;
    position: relative;
    z-index: 9;
  }
  .form-child{
    width: 100%;
    margin-bottom: 16px;
  }
  .form-child label{
    float: left;
    width: 200px;
    text-align: right;
    display: block;
    font-size: 20px;
    color: #969696;
    margin-right: 20px;
    margin-top: 5px;
    font-weight: 300;
  }
  .form-content{
    padding-top: 30px;
  }
  .form-content .form-title{
    font-size: 30px;
    font-weight: 300;
    color: #ff5972;
    margin-bottom: 38px;
  }
  .form-child  .form-control, .form-child  .form-control:focus {
      /* border: none !important; */
    border-bottom: 1px solid #ccc !important;
    border-radius: 0 !important;
    box-shadow: none !important;
    width: calc(100% - 36%);
  }

  .foot-btn{
      text-align: center;
  }
  .foot-btn button{
    padding: 10px 70px;
    background: #ffe1eb;
    color: #b3042f;
    font-weight: 500;
    border: none !important;
  }
  .foot-btn button:hover {
    background: #ff667b;
    color: #fff !important;
}

.login-part{
    /* background: url(images/banner/login-card1.png) no-repeat; */
    height: 100%;
    width: 74%;
    margin: 60px auto;
    text-align: center;
}
.login-part img{
    width: 35em;
}
.login-form{
    position: absolute;
    top: 25%;
    width: 45%;
    margin: 0px 28%;
}
.login-form .form-control, .login-form .form-control:focus {
    display: block;
    width: 100%;
    height: unset !important;
    padding: 10px 8px !important;
    background-color: #f9a3bc3d !important;
    background-clip: padding-box;
    border: none;
    border-radius: 0 !important;
    box-shadow: unset !important;
    text-align: center;
    color: #fff;
    border-bottom: 2px solid #d6044157;
}
.login-form .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
    color: #fff;
}
.login-btn{
    color: #fff !important;
    background-color: #ff5486 !important;
    border-color: #c8a38d00 !important;
 }


 .red-heart-checkbox + label {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-size: 16px;
    padding-left: 10px;
    transition: all 0.2s;
    color: #f1f1f1;
    cursor: pointer;
}
.red-heart-checkbox:checked + label:before {
  content: "";
  color: #ec1c58;
}
.red-heart-checkbox + label:before {
    width: 31.5px;
    height: 30px;
    content: "";
    position: absolute;
    margin-left: -35px;
    margin-top: -3px;
    font-family: FontAwesome;
    font-size: 20px;
    content: "";
    transition: all 0.3s;
    color: #ec1c58;
}
.red-heart-checkbox + label:hover:before {
  transform: scale(1.2);
}
.red-heart-checkbox {
    visibility: hidden;
}
.copyright{
    position: absolute;
    bottom: 0;
    width: 100%;
    text-align: center;
    color:#d6003f ;
}

.social-media{
    position: fixed;
    right: 20px;
    top: 38%;
    z-index: 999;
}
.social-media ul{
    list-style-type: none;
}
.social-media ul li{
    position: relative;
    width: 100%;
    text-align: center;
    cursor: pointer;
    margin-bottom: 16px;
}
.social-media ul li::before{
    content: "\f004";
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: 48px;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: #ff4e80;
   
}
.social-media ul li i{
    position: absolute;
    text-align: center;
    color: #fff;
    left: 19px;
    top: 15px;
}
.social-media ul li:hover{
    transform: scale(1.3);
    transition: all 1s;
}
.social-media ul .facebook:hover::before{content:"\f004"; color: #3b5999 !important; }
.social-media ul .twitter:hover::before{content:"\f004"; color: #55acee !important; }
.social-media ul .instagram:hover::before{content:"\f004"; color: #e4405f !important; }
.social-media ul .whatsapp:hover::before{content:"\f004"; color: #25D366 !important; }
</style>



</head>

<body>

    <!-- <header class="haqto_header haqto_header_transparent">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <nav class="navbar navbar-expand-lg header_nav">
                        <a class="navbar-brand logo" href="#" style="font-family: Pacifico,cursive;">
                              <label>Wedding Bells</label>
                       </a>
                    
                    </nav>
                </div>
            </div>
        </div>
    </header> -->
    <!-- haqto_header -->


    <section class="haqto_banner haqto_banner_one bg_heart">


        <div class="overlay"></div>

        
        <div class="container">
            <div class="login-part reg-part animated zoomIn">
           <img src="<?php echo base_url(); ?>assets/images/banner/login-card2.png">
           <?php if($this->session->flashdata('errorMessage')) { ?>
         <div class="alert alert-danger">
            <?php echo $this->session->flashdata('errorMessage'); ?>
          </div>
         <?php } ?>

           <form action="" method="post" class="login-form">
            <div class="form-group">
              <!-- <label for="email">Email address:</label> -->
              <input type="text" class="form-control wow animated fadeInDown" data-wow-delay=".6s" name="mobile" placeholder="Enter Mobile No" id="email">
            </div>
            <div class="form-group">
              <!-- <label for="pwd">Password:</label> -->
              <input type="password" class="form-control wow animated fadeInDown" data-wow-delay=".77s" name="password" placeholder="Enter password" id="pwd">
            </div>
            <div class="form-group form-check wow animated fadeInDown" data-wow-delay=".88s">
              <div class="form-check-label">
                <input class="form-check-input red-heart-checkbox" id="red-check1" type="checkbox"> <label for='red-check1'>Keep me logged in
                </label>               
            </div>
            </div>
            <button type="submit" name="login" class="btn btn-primary login-btn wow animated fadeInDown" data-wow-delay=".89s">LOGIN</button>
          </form>
        </div>
    </div>

<div class="social-media">
    <ul>
        <li class="facebook"><a href=""> <i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li class="twitter"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li class="instagram"><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        <li class="whatsapp"><a href=""><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
    </ul>
</div>

    <div class="copyright">
        <label>Copyright © 2020 Wedding Bells</label>
    </div>
    </section>
   

    <!-- <footer class="haqto_footer_wrap haqto_footer_light haqto_download_footer haqto_health_footer">
        <div class="haqto_footer_bottom">
            <div class="container">
                <div class="haqto_footer_bottom_inner">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                            <div class="haqto_footer_widget">
                                <a href="#" class="haqto_footer_logo">
                                    <label>Wedding Bells</label>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="haqto_footer_download_menu">
                                <ul class="haqto_footer_menu">
                                    <li><a href="#">Privacy</a></li>
                                    <li><a href="#">Terms</a></li>
                                    <li><a href="#">Reviews</a></li>
                                    <li><a href="#">About </a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                            <div class="haqto_footer_select_language text-right">
                                <select class="select_language">
                                    <option value="0">India - English</option>
                                    <option value="0">United State - English</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer> -->
    <!-- haqto_footer_wrap -->



    

    <script src="<?php echo base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/slick/slick.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/simple-icon/iconify.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/wow/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/sckroller/jquery.parallax-scroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/magnify-pop/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>


<script>
     wow = new WOW(
                      {
                      boxClass:     'wow',      // default
                      animateClass: 'animated', // default
                      offset:       0,          // default
                      mobile:       true,       // default
                      live:         true        // default
                    }
                    )
                    wow.init();
</script>




<script>
 var love = setInterval(function() {
    var r_num = Math.floor(Math.random() * 40) + 1;
    var r_size = Math.floor(Math.random() * 65) + 10;
    var r_left = Math.floor(Math.random() * 100) + 1;
    var r_bg = Math.floor(Math.random() * 25) + 100;
    var r_time = Math.floor(Math.random() * 5) + 5;

    $('.bg_heart').append("<div class='heart' style='width:" + r_size + "px;height:" + r_size + "px;left:" + r_left + "%;background:rgba(255," + (r_bg - 25) + "," + r_bg + ",1);-webkit-animation:love " + r_time + "s ease;-moz-animation:love " + r_time + "s ease;-ms-animation:love " + r_time + "s ease;animation:love " + r_time + "s ease'></div>");

    $('.bg_heart').append("<div class='heart' style='width:" + (r_size - 10) + "px;height:" + (r_size - 10) + "px;left:" + (r_left + r_num) + "%;background:rgba(255," + (r_bg - 25) + "," + (r_bg + 25) + ",1);-webkit-animation:love " + (r_time + 5) + "s ease;-moz-animation:love " + (r_time + 5) + "s ease;-ms-animation:love " + (r_time + 5) + "s ease;animation:love " + (r_time + 5) + "s ease'></div>");

    $('.heart').each(function() {
        var top = $(this).css("top").replace(/[^-\d\.]/g, '');
        var width = $(this).css("width").replace(/[^-\d\.]/g, '');
        if (top <= -100 || width >= 150) {
            $(this).detach();
        }
    });
}, 500);
</script>
<!-- Registeration scripts -->





</body>


</html>