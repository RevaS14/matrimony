<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// require FCPATH . 'vendor/autoload.php';

require_once('ApiServiceController.php');


class ApiController extends ApiServiceController {

    public function __construct() {
		parent::__construct();
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }
    public function index_post() {   
        $response = array(
            "status" => true,
            "message" => "Success"
        );
        $this->response($response, parent::HTTP_OK);
    }


    public function register_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );


        if($postData->UserName != '' 
            && $postData->MobileNumber != '' 
            && $postData->ProfileFor != '' 
            && $postData->Gender != ''  
            && $postData->Password != '' ) {
                
            if(!$this->_mobileExists($postData->MobileNumber)) {
                    if($userId = $this->_saveUser($postData)) {
                        $preferData = array(
                            "userId" => $userId,
                            "CreatedOn" => date('Y-m-d h:i:s')
                        );
                        $this->_insertData('partner_preferencess',$preferData);
                        $token = AUTHORIZATION::generateToken([
                            'UserId' => $userId,
                        ]);
                        $response = array(
                            "status" => true,
                            "message" => 'Successfully Registered.',
                            "token" => $token,
                            'userId' => $userId
                        );
                    } else {
                        $response = array(
                            "status" => false,
                            "message" => 'Something went wrong..'
                        );
                    }
                } else {
                    $response = array(
                        "status" => false,
                        "message" => 'Mobile Number already exists..'
                    );
                }
           
            }

        $this->response($response, parent::HTTP_OK);
    }

    public function register_step_1_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;

        if($postData->DOB != '' 
            && $postData->Religion != '' 
            && $postData->MotherTongue != '') {

            $birthDate = $postData->DOB;
            $dob = date("Y-m-d",strtotime($birthDate));

            $dobObject = new DateTime($dob);
            $nowObject = new DateTime();
    
            $diff = $dobObject->diff($nowObject);
    
           $diff->y;
    

            
            $data = array(
                "DOB" => date('Y-m-d',strtotime($postData->DOB)),
                "Religion" => $postData->Religion,
                "MotherTongue" => $postData->MotherTongue,
                "Age" => $diff->y
            );
            
            $where = array(
                "UserId" => $userId
            );

            $this->_updateData('users',$data,$where);

            $response = array(
                "status" => true,
                "message" => "Updated Successfully",
            );
           
            $this->response($response, parent::HTTP_OK);
        }
    }

    public function register_step_2_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );
        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        if($postData->Caste != '' 
            && $postData->SubCaste != '' 
            && $postData->Gothram != ''
            && $postData->Dosham != '') {
            
            $data = array(
                "Caste" => $postData->Caste,
                "SubCaste" => $postData->SubCaste,
                "Gothram" => $postData->Gothram,
                "Dosham" => $postData->Dosham,
            );
            
            $where = array(
                "UserId" => $userId
            );
            $ex=$this->_getData('users',$where);
            if($ex){
                $this->_updateData('users',$data,$where);

                if($postData->Dosham=='Y') {
                    foreach($postData->Doshams as $row) {
                        $dosham_data = array(
                            "dosham_id" => $row,
                            "user_id" => $userId
                        );
                        $this->_insertData('dosham_users',$dosham_data);
                    }
                }

                $response = array(
                    "status" => true,
                    "message" => "Updated Successfully",
                );

            } else {

                $response = array(
                    "status" => false,
                    "message" => "User Not found",
                );
            }
           

          
           
            $this->response($response, parent::HTTP_OK);
        }
    }

        public function register_step_3_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        if($postData->MaritalStatus != '' 
            && $postData->Height != '' 
            && $postData->FamilyStatus != ''
            && $postData->FamilyType != ''
            && $postData->FamilyValues != ''
            && $postData->AnyDisability != '') {

            $NoOfChildrens = $postData->NoOfChildrens;
            $ChildrensLiveWith = $postData->ChildrensLiveWith;
            if(!$NoOfChildrens) {
                $NoOfChildrens = Null;
            }
            if(!$ChildrensLiveWith) {
                $ChildrensLiveWith = Null;
            }
            
            $data = array(
                "MaritalStatus" => $postData->MaritalStatus,
                "NoOfChildrens" => $NoOfChildrens,
                "ChildrensLiveWith" => $ChildrensLiveWith,
                "Height" => $postData->Height,
                "FamilyStatus" => $postData->FamilyStatus,
                "FamilyType" => $postData->FamilyType,
                "FamilyValues" => $postData->FamilyValues,
                "AnyDisability" => $postData->AnyDisability,
            );
            
            $where = array(
                "UserId" => $userId
            );

            $this->_updateData('users',$data,$where);

            $response = array(
                "status" => true,
                "message" => "Updated Successfully",
            );
           
            $this->response($response, parent::HTTP_OK);
        }
    }

    public function register_step_4_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        if($postData->Education != '' 
            && $postData->EmployedIn != '') {
            
            $data = array(
                "Education" => $postData->Education,
                "EmployedIn" => $postData->EmployedIn,
            );
            if($postData->EmployedIn!='Not Working') {
                if($postData->Occupation) {
                    $data['Occupation'] = $postData->Occupation;
                }
                if($postData->AnnualIncomeCurrency) {
                    $data['AnnualIncomeCurrency'] = $postData->AnnualIncomeCurrency;
                }
                if($postData->AnnualIncome) {
                    $data['AnnualIncome'] = $postData->AnnualIncome;
                }
                if($postData->WorkLocation) {
                    $data['WorkLocation'] = $postData->WorkLocation;
                }
                if($postData->State) {
                    $data['State'] = $postData->State;
                }
                if($postData->City) {
                    $data['City'] = $postData->City;
                }
            }
            
            $where = array(
                "UserId" => $userId
            );

            $this->_updateData('users',$data,$where);

            $response = array(
                "status" => true,
                "message" => "Updated Successfully",
            );
           
            $this->response($response, parent::HTTP_OK);
        }
    }

    public function register_step_5_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        if($postData->AboutMe != '') {
            
            $data = array(
                "AboutMe" => $postData->AboutMe,
            );
            
            $where = array(
                "UserId" => $userId
            );

            $this->_updateData('users',$data,$where);

            $response = array(
                "status" => true,
                "message" => "Updated Successfully",
            );
           
            $this->response($response, parent::HTTP_OK);
        }
    }

    public function saveBasicInfo_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        if($postData->BodyType != '' && $postData->Weight != '' && $postData->College != '') {
            
            $data = array(
                "BodyType" => $postData->BodyType,
                "Weight" => $postData->Weight,
                "College/Ins" => $postData->College,
                "eatingHabits" => $postData->EatingHabits,
                "drinkingHabit" => $postData->DrinkingHabits,
                "smokingHabits" => $postData->SmokingHabits,
                "Star" => $postData->Star,
                "Raasi" => $postData->Raasi,
                "BirthTime" => $postData->BirthTime,
                "BirthCountry" => $postData->BirthCountry,
                "BirthState" => $postData->BirthState,
                "BirthCity" => $postData->BirthCity,
                "FatherStatus" => $postData->FatherStatus,
                "MotherStatus" => $postData->MotherStatus,
                "NoOfBro" => $postData->NoOfBro,
                "NoOfMarriedBro" => $postData->NoOfMarriedBro,
                "NoOfSisters" => $postData->NoOfSisters,
                "NoOfMarriedSisters" => $postData->NoOfMarriedSisters,
                "FamilyLocation" => $postData->FamilyLocation,
                "ParentNo" => $postData->ParentNo,
                "AncestralOrigin" => $postData->AncestralOrigin,
            );

            if($postData->FamilyLocation=='Different') {
                if(isset($postData->FamilyCountry)) {
                    $data['FamilyCountry'] = $postData->FamilyCountry;
                    $data['FamilyState'] = $postData->FamilyState;
                    $data['FamilyCity'] = $postData->FamilyCity;
                }
            }
            
            
            $where = array(
                "UserId" => $userId
            );

            $this->_updateData('users',$data,$where);

            $response = array(
                "status" => true,
                "message" => "Updated Successfully",
            );
           
            $this->response($response, parent::HTTP_OK);
        }
    }

    public function save_preference_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;


        if($postData->AgeFrom != '' 
            && $postData->AgeTo != '' 
            && $postData->HeightFom != '' 
            && $postData->HeightTo != ''  
            && $postData->MaritalStatus != '' 
            && $postData->PhysicalStatus != '' 
            && $postData->EatingHabits != '' 
            && $postData->DrinkingHabits != ''
            && $postData->SmokingHabits != '' ) {

            $data = array (
                "AgeFrom" => $postData->AgeFrom,
                "AgeTo" => $postData->AgeTo,
                "HeightFom" => $postData->HeightFom,
                "HeightTo" => $postData->HeightTo,
                "MaritalStatus" => $postData->MaritalStatus,
                "PhysicalStatus" => $postData->PhysicalStatus,
                "EatingHabits" => $postData->EatingHabits,
                "DrinkingHabits" => $postData->DrinkingHabits,
                "SmokingHabits" => $postData->SmokingHabits
            );

            $where  = array(
                "userId" => $userId
            );

            $this->_updateData('partner_preferencess',$data,$where);

            $response = array(
                "status" => true,
                "message" => 'Preference Saved..'
            );
           
            }

        $this->response($response, parent::HTTP_OK);
    }

    public function generateOrder_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;


        if($postData->amount != '') {
            
            $response = array(
                "status" => true,
                "OrderId" => $this->_generateOrder($postData->amount,$userId)
            );
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function updateProfile_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => $postData,
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;


        if($postData->image != '') {
            
            $base64Decoded = base64_decode($postData->image);
            $image = "uploads/profile/" . $userId . '-DP-' . time() . ".png";
            file_put_contents($image, $base64Decoded);
            $where = array(
                "UserId" => $userId
            );
            $data = array(
                "ProfilePicture" => $image
            );
            $this->_updateData('users',$data,$where);

            $response = array(
                "status" => true,
                "message" => 'Profile Picture Updated..',
            );

        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function locationBasedMatches_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        $data = $this->_getData('users',array('UserId'=>$userId));
        $preference = $this->_getData('partner_preferences',array('UserId'=>$userId));
        if($data->Gender=='Female') {
            $gender = 'Male';
        } else {
            $gender = 'Female';
        }
        if($data->State!='' && $data->City!='') {
            $response = array(
                "status" => true,
                "users" => $this->_getAllDataUser($userId,'UserId!= '.$userId.' and Gender = "'.$gender.'" and State = '.$data->State.' and City = '.$data->City.'')
            );
        } else {
            $response = array(
                "status" => false,
                "message" => "State and city required",
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function starBasedMatches_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        $data = $this->_getData('users',array('UserId'=>$userId));
        $preference = $this->_getData('partner_preferences',array('UserId'=>$userId));
        if($data->Gender=='Female') {
            $gender = 'Male';
        } else {
            $gender = 'Female';
        }
        if($preference->Star!='') {
            $response = array(
                "status" => true,
                "users" => $this->_getAllDataUser($userId,'UserId!= '.$userId.' and Gender = "'.$gender.'" and Star = '.$preference->Star.'')
            );
        } else {
            $response = array(
                "status" => false,
                "message" => "Star required",
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function professionBasedMatches_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        $data = $this->_getData('users',array('UserId'=>$userId));
        $preference = $this->_getData('partner_preferences',array('UserId'=>$userId));
        if($data->Gender=='Female') {
            $gender = 'Male';
        } else {
            $gender = 'Female';
        }
        if($preference->Occupation!='') {
            $response = array(
                "status" => true,
                "users" => $this->_getAllDataUser($userId,'UserId!= '.$userId.' and Gender = "'.$gender.'" and Occupation = '.$preference->Occupation.'')
            );
        } else {
            $response = array(
                "status" => false,
                "message" => "Occupation required",
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function educationBasedMatches_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        $data = $this->_getData('users',array('UserId'=>$userId));
        $preference = $this->_getData('partner_preferences',array('UserId'=>$userId));
        if($data->Gender=='Female') {
            $gender = 'Male';
        } else {
            $gender = 'Female';
        }
        if($preference->Education!='') {
            $response = array(
                "status" => true,
                "users" => $this->_getAllDataUser($userId,'UserId!= '.$userId.' and Gender = "'.$gender.'" and Education = '.$preference->Education.'')          
            );
        } else {
            $response = array(
                "status" => false,
                "message" => "Education required",
            );
        }

        $this->response($response, parent::HTTP_OK);
    }

    public function getPremium_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        $data = $this->_getData('users',array('UserId'=>$userId));
        if($data->Premium=='Y') {
            $response = array(
                "status" => true,
                "message" => "Premium Member",
            );
        } else {
            $response = array(
                "status" => false,
                "message" => "Not a Premium Member",
            );
        }


        $this->response($response, parent::HTTP_OK);
    }

    public function getPremiumMembers_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        $data = $this->_getAllData('users',array('Premium'=>'Y'));
       
            $response = array(
                "status" => true,
                "users" => $data
            );
        


        $this->response($response, parent::HTTP_OK);
    }

    public function viewProfile_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        if($postData->profileId != '') {
            $data = array(
                "user_id" => $userId,
                "profile_id" => $postData->profileId
            );
            $this->_insertData('view_history',$data);
            $response = array(
                "status" => true,
                "message" => 'History Saved..'
            );
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function recentlyViewed_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;

            $response = array(
                "status" => true,
                "users" =>  $this->_getHistory($userId)
            );
        
        $this->response($response, parent::HTTP_OK);
    }

    public function likeHistory_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;

        $response = array(
            "status" => true,
            "users" => $this->_getLikeHistory($userId)
        );
        
        $this->response($response, parent::HTTP_OK);
    }

    public function shortlistHistory_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;

            $response = array(
                "status" => true,
                "users" => $this->_getShortlistHistory($userId)
            );
        
        $this->response($response, parent::HTTP_OK);
    }

    public function getMyLikes_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;

            $response = array(
                "status" => true,
                "users" => $this->_getMyLikes($userId)
            );
        
        $this->response($response, parent::HTTP_OK);
    }

    public function getMyShortlist_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;

            $response = array(
                "status" => true,
                "users" => $this->_getMyShortlist($userId)
            );
        
        $this->response($response, parent::HTTP_OK);
    }

    public function getViewers_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;

            $response = array(
                "status" => true,
                "users" =>  $this->_getViewers($userId)
            );
        
        $this->response($response, parent::HTTP_OK);
    }

    public function likeProfile_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        if($postData->profileId != '') {
            $data = array(
                "user_id" => $userId,
                "profile_id" => $postData->profileId
            );
            $exists = $this->_getData('like_history',$data);
            if($exists) {
                $response = array(
                    "status" => false,
                    "message" => 'Already Liked..'
                );
            } else {
                $this->_insertData('like_history',$data);
                $response = array(
                    "status" => true,
                    "message" => 'Liked successfully..'
                );
            }
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function unlikeProfile_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        if($postData->profileId != '') {
            $data = array(
                "user_id" => $userId,
                "profile_id" => $postData->profileId
            );
            $exists = $this->_getData('like_history',$data);
            if($exists) {
                $this->_deleteData('like_history',$data);
                $response = array(
                    "status" => true,
                    "message" => 'Unliked successfully..'
                );
            } else {
                $response = array(
                    "status" => false,
                    "message" => 'Not found..'
                );
            }
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function shortlistProfile_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        if($postData->profileId != '') {
            $data = array(
                "user_id" => $userId,
                "profile_id" => $postData->profileId
            );
            $exists = $this->_getData('shortlist_history',$data);
            if($exists) {
                $response = array(
                    "status" => false,
                    "message" => 'Already shortlisted..'
                );
            } else {
                $this->_insertData('shortlist_history',$data);
                $response = array(
                    "status" => true,
                    "message" => 'Shortlisted successfully..'
                );
            }
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function removeShortlist_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        if($postData->profileId != '') {
            $data = array(
                "user_id" => $userId,
                "profile_id" => $postData->profileId
            );
            $exists = $this->_getData('shortlist_history',$data);
            if($exists) {
                $this->_deleteData('shortlist_history',$data);
                $response = array(
                    "status" => true,
                    "message" => 'Removed successfully..'
                );
            } else {
                $response = array(
                    "status" => false,
                    "message" => 'Not found..'
                );
            }
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function findMatch_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
            $data = $this->_getData('users',array('UserId'=>$userId));
            if($data) {
                if($data->Gender=='Female') {
                    $gender = 'Male';
                } else {
                    $gender = 'Female';
                }
                
                $response = array(
                    "status" => true,
                    "users" =>  $this->_getAllDataUser($userId,'Gender = '.$gender.'')
                );
            }
          
        
        $this->response($response, parent::HTTP_OK);
    }

    public function religion_preference_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;


        if($postData->Religion != '' 
            && $postData->MotherTongue != '' 
            && $postData->Caste != '' 
            && $postData->SubCaste != ''  
            && $postData->Star != '' 
            && $postData->Dosham != '') {

            $data = array (
                "Religion" => $postData->Religion,
                "MotherTongue" => $postData->MotherTongue,
                "Caste" => $postData->Caste,
                "Star" => $postData->Star,
                "SubCaste" => $postData->SubCaste,
                "Dosham" => $postData->Dosham
            );

            $where = array(
                "UserId" => $userId
            );

            $this->_updateData('partner_preferencess',$data,$where);

            $response = array(
                "status" => true,
                "message" => 'Preference Updated..'
            );
           
            }

        $this->response($response, parent::HTTP_OK);
    }


    public function getPreference_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;


            $where = array(
                "UserId" => $userId
            );

            $response = array(
                "status" => true,
                "data" => $this->_getAllData('partner_preferencess',$where)
            );
           

        $this->response($response, parent::HTTP_OK);
    }

    public function getDropdownValues_get() {
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
        );

        $response = array(
            "status" => true,
            "caste" => $this->_getAllData('caste'),
            "countries" => $this->_getAllData('countries'),
            "states" => $this->_getAllData('states'),
            "cities" => $this->_getAllData('cities'),
            "religion" => $this->_getAllData('religion'),
            "mother_tongue" => $this->_getAllData('mother_tongue'),
            "currency" => $this->_getAllData('currency'),
            "dosham" => $this->_getAllData('dosham'),
            "educations"=> $this->_getAllData('educations'),
            "education_category"=> $this->_getAllData('education_category'),
            "income"=> $this->_getAllData('income'),
            "occupations"=> $this->_getAllData('occupations'),
            "occupation_category"=> $this->_getAllData('occupation_category'),
            "zodiac_signs"=> $this->_getAllData('zodiac_signs'),
            "stars"=> $this->_getAllData('stars'),
        );
           

        $this->response($response, parent::HTTP_OK);
    }

    public function location_preference_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );


        if($postData->Country != '' 
            && $postData->ResidingState != '' 
            && $postData->District != '') {

            $data = array (
                "Country" => $postData->Country,
                "ResidingState" => $postData->ResidingState,
                "District" => $postData->District
            );

            $where = array(
                "UserId" => $userId
            );

            $this->_updateData('partner_preferencess',$data,$where);

            $response = array(
                "status" => true,
                "message" => 'Preference Updated..'
            );
           
            }

        $this->response($response, parent::HTTP_OK);
    }

    public function professional_preference_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "post" => $postData,
        );

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;

        if($postData->Education != '' 
            && $postData->EmployedIn != '' 
            && $postData->Occupation != ''
            && $postData->IncomeMin != ''
            && $postData->IncomeMax != '') {

            $data = array (
                "Education" => $postData->Education,
                "EmployedIn" => $postData->EmployedIn,
                "Occupation" => $postData->Occupation,
                "IncomeMin" => $postData->IncomeMin,
                "IncomeMax" => $postData->IncomeMax
            );

            $where = array(
                "UserId" => $userId
            );

            $this->_updateData('partner_preferencess',$data,$where);

            $response = array(
                "status" => true,
                "message" => 'Preference Updated..'
            );
           
            }

        $this->response($response, parent::HTTP_OK);
    }

    public function login_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        //var_dump($postData);
        if($postData->MobileNumber != '' && $postData->Password != '') {
            $isLogin = $this->_loginUser($postData);
            if($isLogin) {
                $where = array(
                    "UserId" =>$isLogin['UserId']
                );
                $user = $this->_getData('users',$where);
                $data = array(
                    "DeviceId" => $postData->key
                );
                $this->_updateData('users',$data,$where);
                $token = AUTHORIZATION::generateToken([
                    'UserId' => $isLogin['UserId'],
                ]);
                $response = array(
                    "status" => true,
                    "message" => "Logged in successfully..",
                    "token" => $token,
                    "userId" => $isLogin['UserId'],
                    "userName" => $isLogin['UserName'],
                );
            } else {
                $response = array(
                    "status" => false,
                    "message" => "Invalid login credentials.."
                );
            }
        }

        $this->response($response, parent::HTTP_OK);
    }

    private function verifyRequest() {
        // Get all the headers
        $headers = $this->input->request_headers();
        // Extract the token
        $token = (isset($headers['X-Authorization'])) ? $headers['X-Authorization'] : null;
        // Use try-catch
        // JWT library throws exception if the token is not valid
        try {
            // Validate the token
            // Successfull validation will return the decoded user data else returns false
            $data = AUTHORIZATION::validateToken($token);
            if ($data === false) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);
                exit();
            } else {
                return $data;
            }
        } catch (Exception $e) {
            // Token is invalid
            // Send the unathorized access message
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
            exit();
        }
    }

    public function users_get() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );
        $user = $this->_getTable('users');
        if($user) {
            $response = array(
                "status" => true,
                "users" => $user
            );
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function recently_joined_get() {
        $response = array(
            "status" => false,
            "message" => "No user found.."
        );
        $user = $this->_getRecentUsers();
        if($user) {
            $response = array(
                "status" => true,
                "users" => $user
            );
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function get_user_info_get() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        $response = array(
            "status" => false,
            "message" => "Invalid Request",
            "UserId" => $userId
        );
        
        $user = $this->_getAllData('users',array('UserId' => $userId));
        if($user) {
            $response = array(
                "status" => true,
                "users" => $user
            );
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function search_history_get() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );
        $user = $this->_getTable('search_history');
        if($user) {
            $response = array(
                "status" => true,
                "history" => $user
            );
        } 
        $this->response($response, parent::HTTP_OK);
    }

    public function search_user_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        $data = array(
            "UserId" => $userId,
            "keyword" => $postData->userName
        );

        $this->_insertData('search_history',$data);

        $user = $this->_searchUsers($postData->userName);
        if($user) {
            $response = array(
                "status" => true,
                "users" => $user
            );
        } else {
            $response = array(
                "status" => false,
                "message" => "No user found.."
            );
        }
        $this->response($response, parent::HTTP_OK);
    }

    public function isPhonenumberExists_post() {
        $response = array(
            "status" => false,
            "message" => "Invalid Request"
        );

        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);

        $isPhoneExists = $this->_isPhonenumberExists($postData->phone);

        $response = array(
            "status" => ($isPhoneExists >= 1) ? false : true,
            "message" => ($isPhoneExists >= 1) ? 'Already Exists' : 'Valid'
        );

        $this->response($response, parent::HTTP_OK);
    }

    public function sendFCM_post() {
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $postData = json_decode($stream_clean);
        $decoded = $this->verifyRequest();
        $userId = $decoded->UserId;
        // Composer Autoloader
        require FCPATH . 'vendor/autoload.php';
        // Instantiate the client with the project api_token and sender_id.
        $apiToken = "AAAAaW80PhM:APA91bHaAAfLkkXfcLDlK0gmDwKPaaeo3tijrCtCaQsAT3WTSa0FDEYif3yPyo4QT1UwNoDRO2yOulmpViKOm3zTKL3M2rceyUxT985expeWtFmFuPvcIrIBB0eRqIC4bWRjUR9b0lo9";
        $senderId = "452837260819";

        $where = array(
            "UserId" => $postData->userId
        );
        $receiver=$this->_getData('users',$where);
        $deviceId = $receiver->DeviceId;

        $client = new \Fcm\FcmClient($apiToken, $senderId);

        // Instantiate the push notification request object.
        $notification = new \Fcm\Push\Notification();

        $myObjArray = array(
            "userId" => $userId,
            "message" => $postData->message
        );

        // Enhance the notification object with our custom options.
        $notification
            //`->addRecipient('f9Kpc14CTeWH1JEQGK1mnM:APA91bFV-adz_jjEgxcB-qUEATXbXXxRFsMQSU1WaeogaMlYkjjq6PxMjKsxrIygHceUikzJRohtoHBtecVZNzVTfUlJD0jrVDuJIzupl1Xc3acX0oh_lW_my49TdR9cHSLN-pJTagbu')
            ->addRecipient($deviceId)
            ->setTitle($postData->title)
            ->setBody($postData->message)
            ->setColor('#20F037')
            ->setSound("default")
            // ->setIcon("myIcon.png")
            //->addData('userId', $userId);
            ->addDataArray($myObjArray);
            $client->send($notification);
        // Send the notification to the Firebase servers for further handling.
        $response = array(
            "status"=>true,
            "message"=>'Notification Sent Successfully..'
        );
        $this->response($response, parent::HTTP_OK);

    }


}
