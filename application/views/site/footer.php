<footer class="haqto_footer_wrap haqto_footer_light haqto_download_footer haqto_health_footer">
        <div class="haqto_footer_bottom">
            <div class="container">
                <div class="haqto_footer_bottom_inner">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                            <div class="haqto_footer_widget">
                                <a href="#" class="haqto_footer_logo">
                                    <!-- <img src="images/logo.png" alt="logo"> -->
                                    <label>Wedding Bells</label>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="haqto_footer_download_menu">
                                <ul class="haqto_footer_menu">
                                    <li><a href="#">Privacy</a></li>
                                    <li><a href="#">Terms</a></li>
                                    <li><a href="#">Reviews</a></li>
                                    <li><a href="#">About </a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                            <div class="haqto_footer_select_language text-right">
                                <select class="select_language">
                                    <option value="0">India - English</option>
                                    <option value="0">United State - English</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- haqto_footer_wrap -->

    <script src="<?php echo base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/slick/slick.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/simple-icon/iconify.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/wow/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/sckroller/jquery.parallax-scroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/magnify-pop/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
                  var caste = [];
                  $("#caste").select2({
                    data: caste
                  });
              });
      </script>
      <script>
    $(document).ready(function() {
        $("input[name='dosham']").click(function () {
            
        $('#yes-dosham').css('display', ($(this).val() === 'Y') ? 'block':'none');
        if($(this).val()=='N' || $(this).val()=='D') {
            $('input[type=checkbox]').prop('checked',false);
        }
});
});

</script>
<script>
    $(document).ready(function() {
        $("input[name='maritalstatus']").click(function () {
        $('#maritalError').hide();
        $('#NeverMarriedStatus').css('display', ($(this).val() === 'Never Married') ? 'inline-block':'none');
});

$("input[name='maritalstatus']").click(function () {
        $('#OtherStatus').css('display', ($(this).val() !== 'Never Married') ? 'inline-block':'none');
        $('#NoOfChildern').css('display', ($(this).val() !== 'Never Married') ? 'inline-block':'none');
});

$("input[name='NoOfChildern']").click(function () {
    $('#childError').hide();
        $('#ChildrenLiving').css('display', ($(this).val() === 'living') ? 'inline-block':'none');
});
$("input[name='ChildrenLiving']").click(function () {
    $('#childLivingError').hide();
});
$("input[name='FamilyStatus']").click(function () {
    $('#FamilyStatusError').hide();
});
$("input[name='FamilyType']").click(function () {
    $('#FamilyTypeError').hide();
});
$("input[name='FamilyValues']").click(function () {
    $('#FamilyValuesError').hide();
});
$("input[name='Disability']").click(function () {
    $('#DisabilityError').hide();
});
$("input[name='Employed']").click(function () {
    val = $(this).val();
    if(val=='Not Working') {
        $('#occupation').prop('required',false);
        $('#income').prop('required',false);
        $('#WorkLocation').prop('required',false);
        $('#State').prop('required',false);
        $('#City').prop('required',false);
    } else {
        $('#occupation').prop('required',true);
        $('#income').prop('required',true);
        $('#WorkLocation').prop('required',true);
        $('#State').prop('required',true);
        $('#City').prop('required',true);
    }
    $('#EmployedError').hide();
});

});
</script>
<script type="text/javascript">
        $(document).ready(function() {
                  var education = [];
                  var occupation = [];
                  var Income = [];
                  $("#education").select2({
                    data: education
                  });

                  $("#occupation").select2({
                    data: occupation
                  });

                  $("#Income").select2({
                    data: Income
                  });
              });
      </script>




<script>
    $(document).ready(function() {
        $("input[name='Employed']").click(function () {
        $('.education-select').css('display', ($(this).val() !== 'Not Working') ? 'block':'none');
        $('.not-work').addClass('test');
});
    $("#WorkLocation").change(function() {
        val = $('option:selected', this).val();
        $.ajax({
            url: "<?php echo base_url(); ?>/get-state",
            type: 'post',
            dataType: "json",
            data: {
              id: val
            },
            success: function( data ) {
                $.each(data, function( index, value ) {
                    html = "<option value="+value.id+">"+value.name+"</option>";
                    $('#State').append(html);
                });
            }
          });
    });
    $("#State").change(function() {
        val = $('option:selected', this).val();
        $.ajax({
            url: "<?php echo base_url(); ?>/get-cities",
            type: 'post',
            dataType: "json",
            data: {
              id: val
            },
            success: function( data ) {
                $.each(data, function( index, value ) {
                    html = "<option value="+value.id+">"+value.name+"</option>";
                    $('#City').append(html);
                });
            }
          });
    });

});
</script>
<!-- Registeration scripts -->

  
    <script type="text/javascript">		
        $("input[name=registeredby]:radio").change(function() {
        alert('click');
    });
    function regdropdown(el) {
        this.dd = el;
        this.placeholder = this.dd.children('span');
        this.opts = this.dd.find('ul.regdropdown > li');
        this.val = '';
        this.index = -1;
        this.initEvents();
    }
    regdropdown.prototype = {
        initEvents : function() {
            var obj = this;
            obj.dd.on('click', function(event){
                $(this).toggleClass('active');
                return false;
            });
            obj.opts.on('click',function(){
                var opt = $(this);
                obj.val = opt.text();
                obj.index = opt.index();
                obj.placeholder.text(obj.val);
                //alert(obj.index);
                //if(obj.index==2){
                    //$("#regpro"+obj.index).prop("checked", true);
                //}
            });
        },
        getValue : function() {
            return this.val;
        },
        getIndex : function() {
            return this.index;
        }
    }
    
     $(function() {
        var dd = new regdropdown( $('#dd') );
        $(document).click(function() 
        {
         
            $('.desp-drpdwn').removeClass('active');
        });
    });  	
    	
     function InputBoldval(input)
    {  
         document.getElementById(input).style.color = "#000"; 
    }	
    function genopt() 
    {  
            document.getElementById("GEN_VAL").value = '';
            if(document.getElementById("selprofile").innerHTML ==' Myself' || document.getElementById("selprofile").innerHTML =='Relative' || document.getElementById("selprofile").innerHTML =='Friend') 
            {
            $('.gender-border').show();	
            } else {  
            $('.gender-border').hide();	
            }
            
            if(document.getElementById("selprofile").innerHTML =='Son' || document.getElementById("selprofile").innerHTML =='Brother')
            {  
             document.getElementById("GEN_VAL").value = 'M';
                if(document.getElementById("selprofile").innerHTML =='Son') 
                {
                    document.getElementById("gender-err").innerHTML = ""; 	 
                } 
            
                if(document.getElementById("selprofile").innerHTML =='Brother') 
                { 
                    document.getElementById("gender-err").innerHTML = "";  
                } 
            
            } else if(document.getElementById("selprofile").innerHTML =='Daughter' || document.getElementById("selprofile").innerHTML =='Sister') 
            { 
                document.getElementById("GEN_VAL").value = 'F';	
                if(document.getElementById("selprofile").innerHTML =='Daughter') 
                {   
                    document.getElementById("gender-err").innerHTML = "";  
                } 
                if(document.getElementById("selprofile").innerHTML =='Sister') 
                { 
                    document.getElementById("gender-err").innerHTML = "";    
                } 
            
            }    
            document.getElementById("selprofile").style.color = "#000"; 
            name = document.getElementById("selprofile").innerHTML;
            $('#profileFor').val(name);
    }
    function genCheck(gelvalue) 
    { 
        //alert(gelvalue);
        if(gelvalue=='M' && document.getElementById("selprofile").innerHTML ==' Myself'){
        document.getElementById("selprofile").innerHTML = "Myself - Male"; 
        document.getElementById("gender-border").style.display = "none";
        $( "#NAME" ).attr('placeholder',"Enter Name");
        } else if(gelvalue=='M' && document.getElementById("selprofile").innerHTML =='Relative'){
        document.getElementById("selprofile").innerHTML = "Relative - Male"; 
        $( "#NAME" ).attr('placeholder',"Relative's Name");
        document.getElementById("gender-border").style.display = "none"; 				
        } else if(gelvalue=='M' && document.getElementById("selprofile").innerHTML =='Friend'){
        document.getElementById("selprofile").innerHTML = "Friend - Male"; 
        $( "#NAME" ).attr('placeholder',"Friend's Name");
        document.getElementById("gender-border").style.display = "none"; 				 
        } else if(gelvalue=='F' && document.getElementById("selprofile").innerHTML ==' Myself'){
        document.getElementById("selprofile").innerHTML = "Myself - Female";  
         $( "#NAME" ).attr('placeholder',"Enter Name");
        document.getElementById("gender-border").style.display = "none"; 
        } else if(gelvalue=='F' && document.getElementById("selprofile").innerHTML =='Relative'){
        document.getElementById("selprofile").innerHTML = "Relative - Female"; 
        $( "#NAME" ).attr('placeholder',"Relative's Name");
        document.getElementById("gender-border").style.display = "none"; 				
        } else if(gelvalue=='F' && document.getElementById("selprofile").innerHTML =='Friend'){
        document.getElementById("selprofile").innerHTML = "Friend - Female"; 
        $( "#NAME" ).attr('placeholder',"Friend's Name");
        document.getElementById("gender-border").style.display = "none"; 
        } else {
         document.getElementById("gender-border").style.display = "none";
         document.getElementById("gender-err").innerHTML = ""; 
        }
        document.getElementById("selprofile").style.color = "#000"; 	
        //registrationform1.NAME.focus();
        document.getElementById("GENDER").value = gelvalue; 
        document.getElementById("GEN_VAL").value = gelvalue; 			
        document.getElementById("name-border").classList.remove("regis-err");//nn
        document.getElementById("gender-err").innerHTML = "";
        document.getElementById("gender-border").style.display = "none";
         
    }

    
    </script>








<script>
 

var love = setInterval(function() {
    var r_num = Math.floor(Math.random() * 40) + 1;
    var r_size = Math.floor(Math.random() * 65) + 10;
    var r_left = Math.floor(Math.random() * 100) + 1;
    var r_bg = Math.floor(Math.random() * 25) + 100;
    var r_time = Math.floor(Math.random() * 5) + 5;

    $('.bg_heart').append("<dialert('ok');v class='heart' style='width:" + r_size + "px;height:" + r_size + "px;left:" + r_left + "%;background:rgba(255," + (r_bg - 25) + "," + r_bg + ",1);-webkit-animation:love " + r_time + "s ease;-moz-animation:love " + r_time + "s ease;-ms-animation:love " + r_time + "s ease;animation:love " + r_time + "s ease'></div>");

    $('.bg_heart').append("<div class='heart' style='width:" + (r_size - 10) + "px;height:" + (r_size - 10) + "px;left:" + (r_left + r_num) + "%;background:rgba(255," + (r_bg - 25) + "," + (r_bg + 25) + ",1);-webkit-animation:love " + (r_time + 5) + "s ease;-moz-animation:love " + (r_time + 5) + "s ease;-ms-animation:love " + (r_time + 5) + "s ease;animation:love " + (r_time + 5) + "s ease'></div>");

    $('.heart').each(function() {
        var top = $(this).css("top").replace(/[^-\d\.]/g, '');
        var width = $(this).css("width").replace(/[^-\d\.]/g, '');
        if (top <= -100 || width >= 150) {
            $(this).detach();
        }
    });
}, 500);

    $('#submit').click(function(e){
        val = $('#profileFor').val();
        if(val=='') {
            $('#error').html('Please select profile');
                    $('#error').show();
                        setTimeout(function () {
                            $('#error').html('');
                            $('#error').hide();
                        },5000);
            e.preventDefault();
        }
    });
    $('#btn-4').click(function(e){
        val = $("input[name='Employed']:checked").val();
        if(val==null){
            e.preventDefault();
            $('#EmployedError').show();
        }
    });

    $('#btn-3').click(function(e){
        val = $("input[name='maritalstatus']:checked").val();
        if(val==null){
            e.preventDefault();
            $('#maritalError').show();
        } else {
            if(val!='Never Married') {
                childVal = $("input[name='NoOfChildern']:checked").val();
                if(childVal==null){
                    e.preventDefault();
                    $('#childError').show();
                } else {
                    $('#childError').hide();
                    if(childVal!='None'){ 
                        childLivingVal = $("input[name='ChildrenLiving']:checked").val();
                        if(childLivingVal==null){
                            e.preventDefault();
                            $('#childLivingError').show();
                        }
                    }
                }
            }
            $('#maritalError').hide();
        }

        familyStatus = $("input[name='FamilyStatus']:checked").val();
        familyType = $("input[name='FamilyType']:checked").val();
        familyValues = $("input[name='FamilyValues']:checked").val();
        Disability = $("input[name='Disability']:checked").val();
        if(familyStatus==null){
            e.preventDefault();
            $('#FamilyStatusError').show();
        } 
        if(familyType==null){
            e.preventDefault();
            $('#FamilyTypeError').show();
        } 
        if(familyValues==null){
            e.preventDefault();
            $('#FamilyValuesError').show();
        } 
        if(Disability==null){
            e.preventDefault();
            $('#DisabilityError').show();
        } 
        

    });

</script>
<!-- Registeration scripts -->

<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="/__/firebase/8.2.7/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="/__/firebase/8.2.7/firebase-analytics.js"></script>

<!-- Initialize Firebase -->
<script src="/__/firebase/init.js"></script>



</body>


</html>