<?php
class User_model extends CI_Model {

    public function __construct() {
		parent::__construct();
        $this->load->database();
    }

    public function _saveUser($data) {
        $this->db->insert('users',$data);
        return $this->db->insert_id();
    }

    public function _insertData($table, $data) {
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }

    public function _loginUser($postData) {
        $this->db->from('users');
        $this->db->where('MobileNumber',$postData->MobileNumber);
        $query = $this->db->get();
        //var_dump($query);
        if ( $query->num_rows() > 0 ) {
            $row = $query->row();
            
            if (!password_verify($postData->Password, $row->Password)) {
                return false;
            } else {
                if($postData->UID) {
                    $this->db->update('users', array('UID'=>$postData->UID), array('UserId'=>$row->UserId));
                }
                return array(
                    'UserId' => $row->UserId,
                    'UserName' => $row->UserName,
                  );
            }
        }
    }

    public function _searchUsers($username) {
        $this->db->from('users');
        $this->db->like('userName', $username);
        return $this->db->get()->result();
    }

    public function _getTable($table) {
        $this->db->from($table);
        return $this->db->get()->result();
    }

    public function _mobileExists($mobile) {
        $this->db->where('MobileNumber',$mobile);
        return $this->db->get('users')->result();
    }

    public function _isPhonenumberExists($phone) {
		$this->db->select('MobileNumber');
		$this->db->where('MobileNumber', $phone);
		return $this->db->get('users')->num_rows();
    }
    
    public function _deleteUser($id) { 			
		$this->db->where('UserId', $id);
		if($this->db->delete('users')) {
			return true;
		}
    }

    public function _updateData($table,$data,$condition) {
        $this->db->update($table, $data, $condition);
        return true;
    }
    
    public function _findUser($id) {
		return $this->db->get_where('users', array('UserId' => $id))->row();
	}
}