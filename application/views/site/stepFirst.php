
<!doctype html>
<html lang="en">


<!-- Mirrored from droitthemes.com/html/haqto/home_business_apps.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Apr 2020 10:29:15 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Wedding Bells</title>
    <!-- favicon icon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/slick/slick-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/elegant-icon/elegant-icon-style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/linearicons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/magnify-pop/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/animation/animate.css">
    <link href="http://db.onlinewebfonts.com/c/6b6170fe52fb23f505b4e056fefd2679?family=Pacifico" rel="stylesheet" type="text/css"/>
    <!-- custom -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">


   


 
<style>
    .haqto_banner_content .banner_title {
      font-size: 48px;
      color: #737373;
  }
  .haqto_banner.haqto_banner_one .overlay {
    background-color: #ffffff;
    background-image: none;
    opacity: 0.8;
    padding-bottom: 20px;
}
.haqto_banner_content .banner_para {
    color: #adadad;
   }
   .haqto_header .header_nav .haqto_header_menu ul li.nav-item a.nav-link {
    color: #040404;
}
.haqto_header .header_nav .haqto_header_menu ul li.nav-item a.nav-link::before {
    height: 3px;
    background: #ff91aa;
}
.haqto_banner_content .email_box .haqto_btn {
    width: 100%;
    border-radius: 0px;
    background: #fff;
    color: black;
    border: 1px solid #ccc;
    /* box-shadow: 0px 0px 35px #ccccccd6; */
    border-left: 4px solid #FF0066;
}
.haqto_banner_content .email_box .haqto_btn:hover {
    box-shadow: 0px 0px 35px #ccccccd6;
}
.haqto_banner_content{
    background: #ffe1eb;
    height: 100%;
    padding-top: 0px;
}

.haqto_banner_img_content .banner_img {
    position: relative;
    margin-left: -80px;
    display: inline-block;
}
.haqto_banner.haqto_banner_one {
    padding: 170px 0 199px;
    position: relative;
    background: url(images/banner/banner_one_bg_1.png) no-repeat scroll top right;
}
.haqto_footer_social .social_item {
    justify-content: flex-start;
}
.haqto_footer_social .social_item li a {
    color: #989898;
    border: 2px solid #00000057;
}
.haqto_footer_social .social_item li a:hover {
    border-color: #F23264;
    color: #fff;
    background-color: #F23264;
    box-shadow: 0px 10px 10px 3px #ff536c5e;
}


.bg_heart {
    position: relative;
    top: 0;
    left: 0;

    height: 100%;
    overflow: hidden
 }

.heart {
    position: absolute;
    top: -50%;
    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -m-transform: rotate(-45deg);
    transform: rotate(-45deg)
 }

.heart:before {
    position: absolute;
    top: -50%;
    left: 0;
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    background: inherit;
    border-radius: 100%;
}

.heart:after {
    position: absolute;
    top: 0;
    right: -50%;
    display: block;
    content: "";
    width: 100%;
    height: 100%;
    background: inherit;
    border-radius: 100%;
}

@-webkit-keyframes love {
  0%{top:110%}
}
@-moz-keyframes love {
  0%{top:110%}
}
@-ms-keyframes love {
  0%{top:110%}
}
@keyframes love {
  0%{top:110%}
}


.app-store{
    display: inline-block;
    margin-top: 5em;
}
.app-store .play-store {
   margin-right: 20px;
}
.play-store, .apple-store{
    cursor: pointer !important;
}
.app-store .play-store img{
    width: 30%;
}
.app-store .apple-store img{
    width: 26%;
}

.arrow-animate{
    animation: slide1 1s ease-in-out infinite;
    color: #ffffff !important;
    float: right;
    margin-left: 30px;
}

@keyframes slide1 {
    0%,
    100% {
      transform: translate(0, 0);
    }
  
    50% {
      transform: translate(10px, 0);
    }
  }


  .reg-part{
    background: #fff;
    box-shadow: 0px 0px 20px #0000001a;
    padding: 0;
    position: relative;
    z-index: 9;
  }
  .form-child{
    width: 100%;
    margin-bottom: 16px;
  }
  .form-child label{
    float: left;
    width: 200px;
    text-align: right;
    display: block;
    font-size: 20px;
    color: #969696;
    margin-right: 20px;
    margin-top: 5px;
    font-weight: 300;
  }
  .form-content{
    padding-top: 30px;
  }
  .form-content .form-title{
    font-size: 30px;
    font-weight: 300;
    color: #ff5972;
    margin-bottom: 38px;
  }
  .form-child  .form-control, .form-child  .form-control:focus {
      border: none !important;
    border-bottom: 1px solid #ccc !important;
    border-radius: 0 !important;
    box-shadow: none !important;
    width: calc(100% - 50%);
  }

  .foot-btn{
      text-align: center;
  }
  .foot-btn button{
    padding: 10px 70px;
    background: #ffe1eb;
    color: #b3042f;
    font-weight: 500;
    border: none !important;
  }
  .foot-btn button:hover {
    background: #ff667b;
    color: #fff !important;
}
</style>

</head>

<body>

    <header class="haqto_header haqto_header_transparent">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <nav class="navbar navbar-expand-lg header_nav">
                        <a class="navbar-brand logo" href="#" style="font-family: Pacifico,cursive;">
                              <label>Wedding Bells</label>
                       </a>
                        <!-- <button class="navbar-toggler haqto_hamburger" type="button" data-toggle="collapse" data-target="#haqto_header_menu" aria-expanded="false">
                            <span class="bar_icon">
                                <span class="bar bar_1"></span>
                                <span class="bar bar_2"></span>
                                <span class="bar bar_3"></span>
                            </span>
                        </button> -->

                        <!-- <div class="collapse navbar-collapse haqto_header_menu" id="haqto_header_menu">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#"> Home </a>
                                  </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> New Arrival </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> Collections </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"> Categories </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="contact.html">Contact Us</a>
                                </li>
                            </ul>
                        </div> -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- haqto_header -->


    <section class="haqto_banner haqto_banner_one bg_heart">


        <div class="overlay"></div>

        
        <div class="container">
            <div class="reg-part">
            <?php if($this->session->flashdata('errorMessage')) { ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('errorMessage'); ?>
                    </div>
                <?php } ?>
            <div class="row">
                <div class="col-lg-5 col-md-12 col-sm-12 col-12 full_width_1024">
                    <div class="haqto_banner_content left-content">
                       <img src="<?php echo base_url(); ?>assets/images/banner/step1.png" style="width: 100%;">
                    </div>
                </div>
               
                <div class="col-lg-7 col-md-12 col-sm-12 col-12 full_width_1024">
               
                    <div class="haqto_banner_content form-content" style="background: #fff;">
                        <label class="form-title">Please provide us with your basic details</label>
                        <form class="form-inline" method="post">
                          

                            <div class="form-child">
                                <label for="datepicker" >Date Of Birth</label>
                                <input type="date" id="datepicker" name="dob" required class="form-control mb-2 mr-sm-6">
                            </div>


                            <div class="form-child">
                                <label for="Religion" >Religion</label>
                                <select id="Religion" name="religion" required class="form-control mb-2 mr-sm-6" > 
                                     <option value="0">Select</option>
					                 <option value="1">Hindu</option>
					                 <option value="10">Muslim - Shia</option>
					                 <option value="11">Muslim - Sunni</option>
					                 <option value="2">Muslim - Others</option>
					                 <option value="3">Christian</option>
					                 <option value="4">Sikh</option>
					                 <option value="15">Jain - Digambar</option>
					                 <option value="16">Jain - Shwetambar</option>
					                 <option value="5">Jain - Others</option>
					                 <option value="6">Parsi</option>
					                 <option value="7">Buddhist</option>
					                 <option value="8">Inter-Religion</option>
                                </select>
                            </div>

                            <div class="form-child">
                                <label for="Tongue" >Mother Tongue</label>
                                <select id="Tongue" name="motherTongue" required class="form-control mb-2 mr-sm-6" > 
                                    <option value="0">Select</option>
                                    <optgroup label="    --Frequently used--">
                                        <option value="4">Bengali</option>
                                        <option value="14">Gujarati</option>
                                        <option value="17">Hindi</option>
                                        <option value="19">Kannada</option>
                                        <option value="31">Malayalam</option>
                                        <option value="33">Marathi</option>
                                        <option value="34">Marwari</option>
                                        <option value="40">Oriya</option>
                                        <option value="41">Punjabi</option>
                                        <option value="45">Sindhi</option>
                                        <option value="47">Tamil</option>
                                        <option value="48">Telugu</option>
                                        <option value="51">Urdu</option>
                                    </optgroup>
    
                                    <optgroup label="--More--">
                                        <option value="1">Arunachali</option>
                                        <option value="2">Assamese</option>
                                        <option value="3">Awadhi</option>
                                        <option value="5">Bhojpuri</option>
                                        <option value="6">Brij</option>
                                        <option value="7">Bihari</option>
                                        <option value="53">Badaga</option>
                                        <option value="8">Chatisgarhi</option>
                                        <option value="9">Dogri</option>
                                        <option value="10">English</option>
                                        <option value="11">French</option>
                                        <option value="12">Garhwali</option>
                                        <option value="13">Garo</option>
                                        <option value="15">Haryanvi</option>
                                        <option value="16">Himachali/Pahari</option>
                                        <option value="18">Kanauji</option>
                                        <option value="20">Kashmiri</option>
                                        <option value="21">Khandesi</option>
                                        <option value="22">Khasi</option>
                                        <option value="23">Konkani</option>
                                        <option value="24">Koshali</option>
                                        <option value="25">Kumaoni</option>
                                        <option value="26">Kutchi</option>
                                        <option value="27">Lepcha</option>
                                        <option value="28">Ladacki</option>
                                        <option value="29">Magahi</option>
                                        <option value="30">Maithili</option>
                                        <option value="32">Manipuri</option>
                                        <option value="35">Miji</option>
                                        <option value="36">Mizo</option>
                                        <option value="37">Monpa</option>
                                        <option value="38">Nicobarese</option>
                                        <option value="39">Nepali</option>
                                        <option value="42">Rajasthani</option>
                                        <option value="43">Sanskrit</option>
                                        <option value="44">Santhali</option>
                                        <option value="46">Sourashtra</option>
                                        <option value="49">Tripuri</option>
                                        <option value="50">Tulu</option>
                                        <option value="54">Angika</option>
                                    </optgroup>
            
                                </select>
                            </div>
                          
                        <div class="form-child foot-btn">
                            <button class="btn btn-primary mb-2 continue-btn" type="submit" name="submit"> Continue</button>
                        </div>
                          </form>
                                          
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
   










