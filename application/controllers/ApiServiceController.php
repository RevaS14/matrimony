<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require FCPATH . 'vendor/autoload.php';
require FCPATH . 'vendor/getID3/getid3.php';
require FCPATH .'vendor/razorpay-php/config.php';
require FCPATH .'vendor/razorpay-php/Razorpay.php';
use Razorpay\Api\Api;


use chriskacerguis\RestServer\RestController;

class ApiServiceController extends RestController {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('user_model');
        $this->load->model('CommonModel');
    }
    


    public function _saveUser($postData) {
        $data = array(
            "UserName"=>$postData->UserName,
            "MobileNumber"=>$postData->MobileNumber,
            "ProfileFor"=>$postData->ProfileFor,
            "Gender"=>$postData->Gender,
            "Password"=>password_hash($postData->Password,PASSWORD_DEFAULT),
            "CreatedOn"=> date('Y-m-d h:i:s')
        );
        return $this->user_model->_saveUser($data);
    }

    public function _generateOrder($amount,$userId) {
        //$api = new Api('rzp_test_6TnpMxgeTqZxsV', 'xUS52ZVbvRrb3fGLRcOPFwix');
        $api = new Api('rzp_live_H9Y8GhVYfp4If7', '68kaI4srDRSI5x2TyaIXy020');
        $orderData = [
            'receipt'         => 3456,
            'amount'          => $amount*100, // 2000 rupees in paise
            'currency'        => 'INR',
            'payment_capture' => 1 // auto capture
         ];
         
         $razorpayOrder = $api->order->create($orderData);
         $txnData = array(
             "txn_id" => $razorpayOrder['id'],
             "user_id" => $userId,
             "amount" => $amount
         );
         $this->user_model->_insertData('transactions',$txnData);
         
         return $razorpayOrderId = $razorpayOrder['id'];
    }

    public function _insertData($table,$postData) {
        return $this->user_model->_insertData($table,$postData);
    }

    public function _loginUser($postData) {
        return $this->user_model->_loginUser($postData);
    }

    public function _getHistory($userId) {
        return $this->CommonModel->_getHistory($userId);
    }

    public function _getViewers($userId) {
        return $this->CommonModel->_getViewers($userId);
    }


    public function _searchUsers($userName) {
        return $this->user_model->_searchUsers($userName);
    }

    public function _getTable($table) {
        return $this->user_model->_getTable($table);
    }

    public function _getData($table,$where) {
        return $this->CommonModel->_getData($table,$where);
    }

    public function _getAllData($table,$where=array()) {
        return $this->CommonModel->_getAllData($table,$where);
    }

    public function _getAllDataUser($table,$where) {
        return $this->CommonModel->_getAllDataUser($table,$where);
    }

    public function _getShortlistHistory($userId) {
        return $this->CommonModel->_getShortlistHistory($userId);
    }

    public function _getMyLikes($userId) {
        return $this->CommonModel->_getMyLikes($userId);
    }

    public function _getLikeHistory($userId) {
        return $this->CommonModel->_getLikeHistory($userId);
    }

    public function _getMyShortlist($userId) {
        return $this->CommonModel->_getMyShortlist($userId);
    }
    
    public function _getRecentUsers() {
        return $this->CommonModel->_getRecentUsers();
    }

    public function _updateData($table,$data,$where) {
        return $this->user_model->_updateData($table,$data,$where);
    }

    public function _mobileExists($mobile) {
        return $this->user_model->_mobileExists($mobile);
    }

    public function _isPhonenumberExists($phone) {
        return $this->user_model->_isPhonenumberExists($phone);
    }

    public function _deleteUser($userId) {
        return $this->user_model->_deleteUser($userId);
    }

    public function _deleteData($table,$where) {
        return $this->CommonModel->_deleteData($table,$where);
    }

    public function _findUser($id) {
        return $this->user_model->_findUser($id);
    }
    
}
